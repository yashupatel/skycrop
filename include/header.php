<!doctype html>
<html lang="en-US">

<!-- Mirrored from www.skycorp.in/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 04 Jul 2020 05:09:58 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
		<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	
	<!-- This site is optimized with the Yoast SEO Premium plugin v14.4.1 - https://yoast.com/wordpress/plugins/seo/ -->
	<title>Valley Textile</title>
	<meta name="description" content="Hook and loop manufacturer, exporter since 1996. 25+ industries served. Global presence. Customized range of hook and loop fasteners."/>
	<meta name="robots" content="index, follow"/>
	<meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
	<meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
	<link rel="canonical" href="index.html"/>
	<meta property="og:locale" content="en_US"/>
	<meta property="og:type" content="website"/>
	<meta property="og:title" content="Hook and Loop Manufacturer | Hook and Loop Tapes | Sky Industries Ltd"/>
	<meta property="og:description" content="Hook and loop manufacturer, exporter since 1996. 25+ industries served. Global presence. Customized range of hook and loop fasteners."/>
	<meta property="og:url" content="https://www.skycorp.in/"/>
	<meta property="og:site_name" content="Skycorp"/>
	<meta property="article:modified_time" content="2020-07-03T11:14:43+00:00"/>
	<meta property="og:image" content="https://www.skycorp.in/wp-content/uploads/elementor/thumbs/sew_on-tape-480x480-2-orwx7t951b18jl4pj40ou07sjwj23de13zynx53wg0.jpg"/>
	<meta name="twitter:card" content="summary_large_image"/>
	<script type="application/ld+json" class="yoast-schema-graph">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://www.skycorp.in/#organization","name":"Sky Industries Ltd","url":"https://www.skycorp.in/","sameAs":[],"logo":{"@type":"ImageObject","@id":"https://www.skycorp.in/#logo","inLanguage":"en-US","url":"https://www.skycorp.in/wp-content/uploads/2020/05/Untitled-design-11.png","width":100,"height":100,"caption":"Sky Industries Ltd"},"image":{"@id":"https://www.skycorp.in/#logo"}},{"@type":"WebSite","@id":"https://www.skycorp.in/#website","url":"https://www.skycorp.in/","name":"Skycorp","description":"Sky Industries Ltd.","publisher":{"@id":"https://www.skycorp.in/#organization"},"potentialAction":[{"@type":"SearchAction","target":"https://www.skycorp.in/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"ImageObject","@id":"https://www.skycorp.in/#primaryimage","inLanguage":"en-US","url":"https://www.skycorp.in/wp-content/uploads/elementor/thumbs/sew_on-tape-480x480-2-orwx7t951b18jl4pj40ou07sjwj23de13zynx53wg0.jpg"},{"@type":"WebPage","@id":"https://www.skycorp.in/#webpage","url":"https://www.skycorp.in/","name":"Hook and Loop Manufacturer | Hook and Loop Tapes | Sky Industries Ltd","isPartOf":{"@id":"https://www.skycorp.in/#website"},"about":{"@id":"https://www.skycorp.in/#organization"},"primaryImageOfPage":{"@id":"https://www.skycorp.in/#primaryimage"},"datePublished":"2020-03-13T08:00:24+00:00","dateModified":"2020-07-03T11:14:43+00:00","description":"Hook and loop manufacturer, exporter since 1996. 25+ industries served. Global presence. Customized range of hook and loop fasteners.","inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://www.skycorp.in/"]}]}]}</script>
	<!-- / Yoast SEO Premium plugin. -->


<link rel='dns-prefetch' href='http://s.w.org/'/>
<link rel="alternate" type="application/rss+xml" title="Skycorp &raquo; Feed" href=""/>
<link rel="alternate" type="application/rss+xml" title="Skycorp &raquo; Comments Feed" href=""/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.10.4 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script type="text/javascript" data-cfasync="false">var mi_version='7.10.4';var mi_track_user=true;var mi_no_track_reason='';var disableStr='ga-disable-UA-153527602-1';function __gaTrackerIsOptedOut(){return document.cookie.indexOf(disableStr+'=true')>-1;}if(__gaTrackerIsOptedOut()){window[disableStr]=true;}function __gaTrackerOptout(){document.cookie=disableStr+'=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';window[disableStr]=true;}if(mi_track_user){(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');__gaTracker('create','UA-153527602-1','auto');__gaTracker('set','forceSSL',true);__gaTracker('require','displayfeatures');__gaTracker('send','pageview');}else{console.log("");(function(){var noopfn=function(){return null;};var noopnullfn=function(){return null;};var Tracker=function(){return null;};var p=Tracker.prototype;p.get=noopfn;p.set=noopfn;p.send=noopfn;var __gaTracker=function(){var len=arguments.length;if(len===0){return;}var f=arguments[len-1];if(typeof f!=='object'||f===null||typeof f.hitCallback!=='function'){console.log('Not running function __gaTracker('+arguments[0]+" ....) because you are not being tracked. "+mi_no_track_reason);return;}try{f.hitCallback();}catch(ex){}};__gaTracker.create=function(){return new Tracker();};__gaTracker.getByName=noopnullfn;__gaTracker.getAll=function(){return[];};__gaTracker.remove=noopfn;window['__gaTracker']=__gaTracker;})();}</script>
<!-- / Google Analytics by MonsterInsights
<noscript><link rel='stylesheet' href='https://www.skycorp.in/wp-content/themes/blocksy/static/bundle/no-scripts.css' type='text/css'/></noscript> -->

<link rel="stylesheet" href="css/style1.css" >
<style id="ct-main-styles-inline-css" type="text/css">.ct-header-account {--margin:0 10px 0 20px !important;--linkInitialColor:var(--color);--iconSize:15px;--avatarSize:22px;}#account-modal {--backgroundColor:rgba(18, 21, 25, 0.7);--backgroundImage:none;}header [data-row="bottom"] {--boxShadow:0px 25px 40px rgba(0,0,0,0.03);--height:60px;}[data-header*="static"] [data-row="bottom"] {--backgroundColor:rgba(255, 255, 255, 0);--backgroundImage:none;}header [data-row="bottom"][data-border] {--borderBottom:none;--borderTop:1px solid #f2f1f1;}.ct-header-cta .ct-button-ghost {--buttonTextHoverColor:#ffffff;--buttonTextInitialColor:var(--buttonInitialColor);}.ct-header-cart i {--iconSize:15px;}.ct-header-cart > a {--linkInitialColor:var(--color);}.ct-header-cart {--margin:0 0 0 20px !important;--cartBadgeBackground:var(--paletteColor2);--cartBadgeText:#ffffff;}.ct-cart-content {--backgroundColor:#202628;--linkInitialColor:#ffffff;--dropdownTopOffset:15px;}.custom-logo-link {--maxHeight:50px;}.site-title {--fontSize:25px;--linkInitialColor:var(--paletteColor4);}.site-description {--fontWeight:500;--fontSize:13px;}.header-menu-1 {--menuItemsSpacing:50px;--menuIndicatorActiveColor:var(--paletteColor1);}.header-menu-1 > ul {--height:100%;}.header-menu-1> ul > li > a {--fontSize:14px;--textTransform:uppercase;--fontWeight:700;--lineHeight:1.3;}.header-menu-1 > ul > li > a {--colorHoverType3:#ffffff;--linkInitialColor:#000000;--linkHoverColor:var(--paletteColor2);}.header-menu-1 .sub-menu {--borderRadius:2px;--backgroundColor:#889093;--linkHoverColor:#000000;--fontSize:14px;--marginTop:15px;--width:200px;--fontWeight:500;--linkInitialColor:#ffffff;--dropDownDivider:1px dashed rgba(255,255,255,0.17);--boxShadow:0px 10px 20px rgba(41, 51, 61, 0.1);}.header-menu-1 .sub-menu li {--padding:13px;}.header-menu-2 {--menuItemsSpacing:25px;--menuIndicatorActiveColor:var(--paletteColor1);}.header-menu-2 > ul {--height:100%;}.header-menu-2> ul > li > a {--fontSize:12px;--textTransform:uppercase;--fontWeight:700;--lineHeight:1.3;}.header-menu-2 > ul > li > a {--colorHoverType3:#ffffff;--linkInitialColor:var(--color);}.header-menu-2 .sub-menu {--borderRadius:2px;--backgroundColor:#29333C;--fontSize:12px;--marginTop:15px;--width:200px;--fontWeight:500;--linkInitialColor:#ffffff;--dropDownDivider:1px dashed rgba(255, 255, 255, 0.1);--boxShadow:0px 10px 20px rgba(41, 51, 61, 0.1);}.header-menu-2 .sub-menu li {--padding:13px;}header [data-row="middle"] {--height:70px;}[data-header*="static"] [data-row="middle"] {--backgroundColor:rgba(255, 255, 255, 0);--backgroundImage:none;}header [data-row="middle"][data-border] {--borderBottom:none;--borderTop:none;}.mobile-menu {--fontSize:30px;--fontWeight:700;--linkInitialColor:#ffffff;}#offcanvas {--backgroundColor:rgba(18, 21, 25, 0.98);--backgroundImage:none;}[data-behaviour*="side"] {--boxShadow:0px 0px 70px rgba(0, 0, 0, 0.35);}.ct-header-search {--linkInitialColor:var(--color);--iconSize:15px;}#search-modal {--backgroundColor:rgba(18, 21, 25, 0.98);--linkInitialColor:#ffffff;--backgroundImage:none;}.ct-header-socials {--iconSize:15px;--spacing:30px;}.ct-header-socials [data-color="custom"] {--backgroundColorHover:var(--paletteColor1);--linkInitialColor:var(--color);--backgroundColor:rgba(218, 222, 228, 0.3);}.ct-header-socials .ct-label {--visibility:none;}.ct-header-text {--linkInitialColor:#0a0a0a;--fontSize:16px;--maxWidth:100%;--fontWeight:700;--lineHeight:1.3;--color:#000000;--linkHoverColor:var(--paletteColor2);}header [data-row="top"] {--height:50px;}[data-header*="static"] [data-row="top"] {--backgroundColor:#f9f9f9;--backgroundImage:none;}header [data-row="top"][data-border] {--borderBottom:none;--borderTop:none;}.ct-header-trigger {--secondColor:#eeeeee;--linkInitialColor:var(--color);--secondColorHover:#eeeeee;}[data-header] {--backgroundColor:#ffffff;--backgroundImage:none;}.site-footer [data-row="bottom"] > div {--gridTemplateColummns:initial;--itemsGap:60px;--containerSpacing:7px;}.site-footer [data-row="bottom"] .widget-title {--fontSize:16px;}.site-footer [data-row="bottom"] .ct-widget {--linkInitialColor:var(--color);}.site-footer [data-row="bottom"] [data-divider="columns"] {--border:none;}.site-footer [data-row="bottom"] {--backgroundColor:#a8e3fc;--borderBottom:none;--borderTop:none;--backgroundImage:none;}.ct-footer-copyright {--fontSize:15px;--fontWeight:400;--lineHeight:1.3;--color:#3f3f3f;}[data-column="copyright"] {--vertical-alignment:center;--horizontal-alignment:center;}.footer-menu {--menuItemsSpacing:25px;}[data-column="menu"] {--vertical-alignment:center;--alignment:center;}.footer-menu ul {--fontSize:18px;--textTransform:uppercase;--fontWeight:700;--lineHeight:1.3;}.footer-menu > ul > li > a {--linkHoverColor:#000000;--linkInitialColor:var(--color);}.site-footer [data-row="middle"] > div {--gridTemplateColummns:initial;--itemsGap:60px;--containerSpacing:10px;}.site-footer [data-row="middle"] .widget-title {--headingColor:var(--paletteColor5);--fontSize:16px;}.site-footer [data-row="middle"] .ct-widget > *:not(.widget-title) {--color:rgba(255,255,255,0.6);}.site-footer [data-row="middle"] .ct-widget {--linkInitialColor:var(--paletteColor5);}.site-footer [data-row="middle"] [data-divider="columns"] {--border:none;}.site-footer [data-row="middle"] {--backgroundColor:#a8e3fc;--borderBottom:none;--borderTop:none;--backgroundImage:none;}.ct-footer-socials {--iconSize:34px;--spacing:20px;}[data-column="socials"] {--vertical-alignment:flex-end;--horizontal-alignment:center;}.ct-footer-socials [data-color="custom"] {--backgroundColorHover:var(--paletteColor1);--linkHoverColor:#000000;--linkInitialColor:var(--color);--backgroundColor:rgba(218, 222, 228, 0.3);}.ct-footer-socials .ct-label {--visibility:none;}.site-footer [data-row="top"] > div {--gridTemplateColummns:repeat(2, 1fr);--itemsGap:60px;--containerSpacing:35px;}.site-footer [data-row="top"] .widget-title {--fontSize:16px;}.site-footer [data-row="top"] .ct-widget {--linkHoverColor:#85cbe9;--linkInitialColor:var(--color);}.site-footer [data-row="top"] [data-divider="columns"] {--border:none;}.site-footer [data-row="top"] {--backgroundColor:#a8e3fc;--borderBottom:none;--borderTop:none;--backgroundImage:none;}.ct-footer-reveal .site-footer {--position:sticky;}.site-footer {--backgroundColor:#a8e3fc;--backgroundImage:none;}.ct-comments-container {--backgroundColor:#f8f9fb;--narrowContainer:750px;--backgroundImage:none;}.ct-pagination {--spacing:80px;}.ct-pagination[data-divider] {--border:none;}.ct-pagination[data-type="simple"] {--colorActive:#ffffff;}.content-area {overflow:hidden;}.entry-card .entry-title {--fontSize:20px;--lineHeight:1.3;}.entry-excerpt {--cardExcerptSize:16px;}.entry-card .entry-meta {--fontSize:12px;--textTransform:uppercase;--fontWeight:600;--lineHeight:1.3;}.entry-button[data-type="outline"] {--colorHover:#ffffff;}[data-cards="boxed"] .entry-card {--boxShadow:0px 12px 18px -6px rgba(34, 56, 101, 0.04);--cardSpacing:35px;--cardBackground:#ffffff;--border:none;}[data-cards="simple"] .entry-card {--border:1px dashed rgba(224, 229, 235, 0.8);}.entries {--cardsGap:30px;}</style>
<style id="ct-main-styles-tablet-inline-css" type="text/css" media="(max-width: 1000px) and (min-width: 690px)">.ct-header-account {--margin:0 10px 0 20px !important;--linkInitialColor:var(--color);--iconSize:15px;--avatarSize:22px;}header [data-row="bottom"] {--boxShadow:0px 25px 40px rgba(0,0,0,0.03);--height:60px;}[data-header*="static"] [data-row="bottom"] {--backgroundColor:rgba(255, 255, 255, 0);--backgroundImage:none;}header [data-row="bottom"][data-border] {--borderBottom:none;--borderTop:1px solid #f2f1f1;}.ct-header-cta .ct-button-ghost {--buttonTextHoverColor:#ffffff;--buttonTextInitialColor:var(--buttonInitialColor);}.ct-header-cart i {--iconSize:15px;}.ct-header-cart > a {--linkInitialColor:var(--color);}.ct-header-cart {--cartBadgeBackground:var(--paletteColor2);--cartBadgeText:#ffffff;}.custom-logo-link {--maxHeight:50px;}.site-title {--fontSize:25px;--linkInitialColor:var(--paletteColor4);}.site-description {--fontSize:13px;}.header-menu-1> ul > li > a {--fontSize:14px;--lineHeight:1.3;}.header-menu-1 .sub-menu {--borderRadius:2px;--fontSize:14px;--boxShadow:0px 10px 20px rgba(41, 51, 61, 0.1);}.header-menu-2> ul > li > a {--fontSize:12px;--lineHeight:1.3;}.header-menu-2 .sub-menu {--borderRadius:2px;--fontSize:12px;--boxShadow:0px 10px 20px rgba(41, 51, 61, 0.1);}header [data-row="middle"] {--height:70px;}[data-header*="static"] [data-row="middle"] {--backgroundColor:rgba(255, 255, 255, 0);--backgroundImage:none;}header [data-row="middle"][data-border] {--borderBottom:none;--borderTop:none;}.mobile-menu {--fontSize:30px;}[data-behaviour*="side"] {--boxShadow:0px 0px 70px rgba(0, 0, 0, 0.35);}.ct-header-search {--linkInitialColor:var(--color);--iconSize:15px;}.ct-header-socials {--iconSize:20px;--spacing:30px;}.ct-header-socials [data-color="custom"] {--backgroundColorHover:var(--paletteColor1);--linkInitialColor:var(--paletteColor5);--backgroundColor:rgba(218, 222, 228, 0.3);}.ct-header-socials .ct-label {--visibility:none;}.ct-header-text {--linkInitialColor:#0a0a0a;--fontSize:16px;--maxWidth:100%;--lineHeight:1.3;--color:#000000;--linkHoverColor:var(--paletteColor2);}header [data-row="top"] {--height:50px;}[data-header*="static"] [data-row="top"] {--backgroundColor:#f9f9f9;--backgroundImage:none;}header [data-row="top"][data-border] {--borderBottom:none;--borderTop:none;}.site-footer [data-row="bottom"] > div {--gridTemplateColummns:initial;--itemsGap:60px;--containerSpacing:25px;}.site-footer [data-row="bottom"] .widget-title {--fontSize:16px;}.ct-footer-copyright {--lineHeight:1.3;--fontSize:15px;}[data-column="copyright"] {--vertical-alignment:center;--horizontal-alignment:center;}.footer-menu {--menuItemsSpacing:25px;}[data-column="menu"] {--vertical-alignment:center;--alignment:center;}.footer-menu ul {--fontSize:18px;--lineHeight:1.3;}.site-footer [data-row="middle"] > div {--gridTemplateColummns:initial;--itemsGap:60px;--containerSpacing:50px;}.site-footer [data-row="middle"] .widget-title {--fontSize:16px;}.ct-footer-socials {--iconSize:34px;--spacing:20px;}[data-column="socials"] {--vertical-alignment:flex-end;--horizontal-alignment:center;}.ct-footer-socials [data-color="custom"] {--backgroundColorHover:var(--paletteColor1);--linkHoverColor:#000000;--linkInitialColor:var(--color);--backgroundColor:rgba(218, 222, 228, 0.3);}.ct-footer-socials .ct-label {--visibility:none;}.site-footer [data-row="top"] > div {--gridTemplateColummns:initial;--itemsGap:60px;--containerSpacing:35px;}.site-footer [data-row="top"] .widget-title {--fontSize:16px;}.ct-footer-reveal .site-footer {--position:static;}.ct-footer-reveal .site-main {--boxShadow:none;}.ct-pagination {--spacing:60px;}.entry-card .entry-title {--fontSize:20px;--lineHeight:1.3;}.entry-excerpt {--cardExcerptSize:16px;}.entry-card .entry-meta {--fontSize:12px;--lineHeight:1.3;}.entries {--cardsGap:30px;}[data-cards="boxed"] .entry-card {--boxShadow:0px 12px 18px -6px rgba(34, 56, 101, 0.04);--cardSpacing:35px;}</style>
<style id="ct-main-styles-mobile-inline-css" type="text/css" media="(max-width: 690px)">.ct-header-account {--margin:0 10px 0 20px !important;--linkInitialColor:var(--color);--iconSize:15px;--avatarSize:22px;}header [data-row="bottom"] {--boxShadow:0px 25px 40px rgba(0,0,0,0.03);--height:60px;}[data-header*="static"] [data-row="bottom"] {--backgroundColor:rgba(255, 255, 255, 0);--backgroundImage:none;}header [data-row="bottom"][data-border] {--borderBottom:none;--borderTop:1px solid #f2f1f1;}.ct-header-cta .ct-button-ghost {--buttonTextHoverColor:#ffffff;--buttonTextInitialColor:var(--buttonInitialColor);}.ct-header-cart i {--iconSize:15px;}.ct-header-cart > a {--linkInitialColor:var(--color);}.ct-header-cart {--cartBadgeBackground:var(--paletteColor2);--cartBadgeText:#ffffff;}.custom-logo-link {--maxHeight:50px;}.site-title {--fontSize:25px;--linkInitialColor:var(--paletteColor4);}.site-description {--fontSize:12px;}.header-menu-1> ul > li > a {--fontSize:14px;--lineHeight:1.3;}.header-menu-1 .sub-menu {--borderRadius:2px;--fontSize:14px;--boxShadow:0px 10px 20px rgba(41, 51, 61, 0.1);}.header-menu-2> ul > li > a {--fontSize:12px;--lineHeight:1.3;}.header-menu-2 .sub-menu {--borderRadius:2px;--fontSize:12px;--boxShadow:0px 10px 20px rgba(41, 51, 61, 0.1);}header [data-row="middle"] {--height:70px;}[data-header*="static"] [data-row="middle"] {--backgroundColor:rgba(255, 255, 255, 0);--backgroundImage:none;}header [data-row="middle"][data-border] {--borderBottom:none;--borderTop:none;}.mobile-menu {--fontSize:23px;}[data-behaviour*="side"] {--boxShadow:0px 0px 70px rgba(0, 0, 0, 0.35);}.ct-header-search {--linkInitialColor:var(--color);--iconSize:15px;}.ct-header-socials {--iconSize:20px;--spacing:30px;}.ct-header-socials [data-color="custom"] {--backgroundColorHover:var(--paletteColor1);--linkInitialColor:var(--paletteColor5);--backgroundColor:rgba(218, 222, 228, 0.3);}.ct-header-socials .ct-label {--visibility:none;}.ct-header-text {--linkInitialColor:#0a0a0a;--fontSize:16px;--maxWidth:100%;--lineHeight:1.3;--color:#000000;--linkHoverColor:var(--paletteColor2);}header [data-row="top"] {--height:50px;}[data-header*="static"] [data-row="top"] {--backgroundColor:#f9f9f9;--backgroundImage:none;}header [data-row="top"][data-border] {--borderBottom:none;--borderTop:none;}.site-footer [data-row="bottom"] > div {--gridTemplateColummns:initial;--itemsGap:60px;--containerSpacing:15px;}.site-footer [data-row="bottom"] .widget-title {--fontSize:16px;}.ct-footer-copyright {--lineHeight:1.3;--fontSize:15px;}[data-column="copyright"] {--vertical-alignment:center;--horizontal-alignment:center;}.footer-menu {--menuItemsSpacing:20px;}[data-column="menu"] {--vertical-alignment:center;--alignment:center;}.footer-menu ul {--fontSize:18px;--lineHeight:1.3;}.site-footer [data-row="middle"] > div {--gridTemplateColummns:initial;--itemsGap:60px;--containerSpacing:40px;}.site-footer [data-row="middle"] .widget-title {--fontSize:16px;}.ct-footer-socials {--iconSize:34px;--spacing:20px;}[data-column="socials"] {--vertical-alignment:flex-end;--horizontal-alignment:center;}.ct-footer-socials [data-color="custom"] {--backgroundColorHover:var(--paletteColor1);--linkHoverColor:#000000;--linkInitialColor:var(--color);--backgroundColor:rgba(218, 222, 228, 0.3);}.ct-footer-socials .ct-label {--visibility:none;}.site-footer [data-row="top"] > div {--gridTemplateColummns:initial;--itemsGap:60px;--containerSpacing:35px;}.site-footer [data-row="top"] .widget-title {--fontSize:16px;}.ct-footer-reveal .site-footer {--position:static;}.ct-footer-reveal .site-main {--boxShadow:none;}.ct-pagination {--spacing:50px;}.entry-card .entry-title {--fontSize:18px;--lineHeight:1.3;}.entry-excerpt {--cardExcerptSize:16px;}.entry-card .entry-meta {--fontSize:12px;--lineHeight:1.3;}.entries {--cardsGap:30px;}[data-cards="boxed"] .entry-card {--boxShadow:0px 12px 18px -6px rgba(34, 56, 101, 0.04);--cardSpacing:25px;}</style>
<link rel="stylesheet" href="css/style3.css" >
<link rel="stylesheet" href="css/style4.css" >
<link rel="stylesheet" href="css/style5.css" >
<link rel="stylesheet" href="css/style6.css" >
<link rel="stylesheet" href="css/style7.css" >
<link rel="stylesheet" href="css/style8.css" >
<link rel="stylesheet" href="css/style9.css" >
<link rel="stylesheet" href="css/style10.css" >
<link rel="stylesheet" href="css/style11.css" >


<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>

<link rel='stylesheet' id='eael-front-end-css' href='https://www.skycorp.in/wp-content/uploads/essential-addons-elementor/fa14246f25c1f49521caa3b833063c08.min.css?ver=1593839398' type='text/css' media='all'/>

<link rel="stylesheet" href="css/style12.css" >
<link rel="stylesheet" href="css/style13.css" >
<!-- <link rel='stylesheet' id='parent-style-css' href='https://www.skycorp.in/wp-content/themes/blocksy/style.css?ver=5.4.2' type='text/css' media='all'/> -->
<link rel="stylesheet" href="css/style14.css" >
<link rel="stylesheet" href="css/style15.css" >
<link rel="stylesheet" href="css/style16.css" >
<link rel="stylesheet" href="css/style17.css" >
<link rel="stylesheet" href="css/style18.css" >
<link rel="stylesheet" href="css/style19.css" >
<link rel="stylesheet" href="css/style20.css" >
<link rel="stylesheet" href="css/style21.css" >
<link rel="stylesheet" href="css/style22.css" >

<script type="text/template" id="tmpl-variation-template">
	<div class="woocommerce-variation-description">{{{ data.variation.variation_description }}}</div>
	<div class="woocommerce-variation-price">{{{ data.variation.price_html }}}</div>
	<div class="woocommerce-variation-availability">{{{ data.variation.availability_html }}}</div>
</script>

<script type="text/template" id="tmpl-unavailable-variation-template">
	<p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>

<script type='text/javascript'>//<![CDATA[
var monsterinsights_frontend={"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/www.skycorp.in","hash_tracking":"false"};
//]]></script>
<script type='text/javascript' src="javascript/js1.js" ></script>
<script type='text/javascript' src="javascript/js2.js" ></script>
<script type='text/javascript' src="javascript/js3.js" ></script>
<script type='text/javascript' src="javascript/js4.js" ></script>
<script type='text/javascript' src="javascript/js5.js" ></script>


<link rel='https://api.w.org/' href='wp-json/index.html'/>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd"/>
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.skycorp.in/wp-includes/wlwmanifest.xml"/> 
<meta name="generator" content="WordPress 5.4.2"/>
<meta name="generator" content="WooCommerce 4.1.0"/>
<link rel='shortlink' href='index.html'/>
<link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed59d8.json?url=https%3A%2F%2Fwww.skycorp.in%2F"/>
<link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embedd2c2?url=https%3A%2F%2Fwww.skycorp.in%2F&amp;format=xml"/>
	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<!-- There is no amphtml version available for this URL. --><link rel="icon" href="https://www.skycorp.in/wp-content/uploads/2020/05/output-onlinepngtools-31-100x100.png" sizes="32x32"/>
<link rel="icon" href="https://www.skycorp.in/wp-content/uploads/2020/05/output-onlinepngtools-31.png" sizes="192x192"/>
<link rel="apple-touch-icon" href="https://www.skycorp.in/wp-content/uploads/2020/05/output-onlinepngtools-31.png"/>
<meta name="msapplication-TileImage" content="https://www.skycorp.in/wp-content/uploads/2020/05/output-onlinepngtools-31.png"/>

<link rel="stylesheet" href="css/style23.css" >

			</head>

<body data-rsssl=1 class="home page-template-default page page-id-14 theme-blocksy woocommerce-no-js ct-loading ct-primary-spacing ct-footer-reveal elementor-default elementor-kit-379 elementor-page elementor-page-14" data-link="type-1" data-forms="classic">


<div id="main-container">
	<header id="header" data-header="static" data-device="desktop"><div data-row="middle" data-columns="3"><div class="ct-container"><div data-column="start" data-placements="1"><div data-items="primary">
<<<<<<< HEAD

=======
<div class="ct-header-text " data-id="text">
	<div class="entry-content">
		<p><a class="btn-tel" href="tel:8775321561"><img class="alignnone  wp-image-486" style="margin-top: 10px;" src="images/logo2.png"  width="100" height="100"/> </a></p>	</div>
</div>
>>>>>>> 88c47974ff5476a27aa7ed9ffbaacab51299dcec

<div class="ct-header-cta " data-id="button">

	<a href="request-a-quote/index.php" class="ct-button" data-size="medium" target="_blank">
		Request A Quote	</a>
</div>
<<<<<<< HEAD
</div></div>
=======
</div></div><div data-column="middle"><div data-items="">
<div class="site-branding" data-id="logo">

	<h1 class="site-title">
					<!-- <a href="index.php" class="custom-logo-link" rel="home"><img width="2598" height="1181" src="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-logo-2.png" class="custom-logo" alt="Skycorp"/></a>			</h1> -->

	</div>

</div></div><div data-column="end" data-placements="1"><div data-items="primary">
<div class="ct-header-search " data-id="search">

	<a href="#search-modal">
		<svg viewBox="0 0 10 10">
			<path d="M9.7,9.7c-0.4,0.4-1,0.4-1.3,0L6.7,8.1C6.1,8.5,5.3,8.8,4.4,8.8C2,8.8,0,6.8,0,4.4S2,0,4.4,0s4.4,2,4.4,4.4
		c0,0.9-0.3,1.7-0.7,2.4l1.7,1.7C10.1,8.8,10.1,9.4,9.7,9.7z M4.4,1.3c-1.7,0-3.1,1.4-3.1,3.1s1.4,3.1,3.1,3.1c1.7,0,3.1-1.4,3.1-3.1
		S6.1,1.3,4.4,1.3z"/>
		</svg>
		<span hidden>Search</span>
	</a>
</div>

<div class="ct-header-account" data-id="account">

	
			<a href="#account-modal">
			<svg width="15" height="15" viewBox="0 0 20 20">
				<path d="M10 0C4.5 0 0 4.5 0 10s4.5 10 10 10 10-4.5 10-10S15.5 0 10 0zm0 2c4.4 0 8 3.6 8 8 0 1.6-.5 3.1-1.3 4.3l-.8-.6C14.4 12.5 11.5 12 10 12s-4.4.5-5.9 1.7l-.8.6C2.5 13.1 2 11.6 2 10c0-4.4 3.6-8 8-8zm0 1.8C8.2 3.8 6.8 5.2 6.8 7c0 1.8 1.5 3.3 3.2 3.3s3.2-1.5 3.2-3.3c0-1.8-1.4-3.2-3.2-3.2zm0 1.9c.7 0 1.2.6 1.2 1.2 0 .7-.6 1.2-1.2 1.2S8.8 7.7 8.8 7c0-.7.5-1.3 1.2-1.3zm0 8.3c3.1 0 4.8 1.2 5.5 1.8-1.4 1.3-3.3 2.2-5.5 2.2s-4.1-.9-5.5-2.2c.7-.6 2.4-1.8 5.5-1.8zm-5.9 1.3c.1.1.2.3.4.4-.2-.1-.3-.2-.4-.4zm11.8.1c-.1.1-.2.2-.3.4.1-.2.2-.3.3-.4z"/>
			</svg>
>>>>>>> 88c47974ff5476a27aa7ed9ffbaacab51299dcec

<div data-row="bottom" data-columns="1" data-border="top-fixed"><div class="ct-container"><div data-column="middle">
<div class="site-branding" data-id="logo">
		<a href="index.php" class="custom-logo-link" rel="home"><img width="100" height="150" src="/unnamed.png"  alt="Skycorp"/></a>			
	</div>
	<div data-items="">

<nav id="header-menu-1" class="header-menu-1" data-id="menu" data-type="type-1" data-dropdown-animation="type-3" data-responsive>

	<ul id="menu-main-menu" class="menu"><li id="menu-item-369" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item menu-item-369"><a href="index.php" aria-current="page">Home</a></li>
<li id="menu-item-243" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-243"><a href="#">Products<span class="child-indicator"><svg width="4" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
	<li id="menu-item-418" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-418"><a href="#">Hook & Look<span class="child-indicator"><svg width="0" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li id="menu-item-1498" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1498"><a href="Nilon_Polister_Mix_Tap.php">Nylon Polyester Mix Tape</a></li>
		<li id="menu-item-600" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-600"><a href="high_strip_tap.php">High Strip Tape</a></li>
		<li id="menu-item-601" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-601"><a href="fire_retardant.php">Fire Retardant</a></li>
		<li id="menu-item-1257" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1257"><a href="100_nylon_tap.php">100% Nylon Tape</a></li>
		<li id="menu-item-1258" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1258"><a href="soft_hook.php">Soft Hook Tape</a></li>
		<li id="menu-item-628" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-628"><a href="unnapped.php">Unnapped Loop</a></li>
		<li id="menu-item-629" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-629"><a href="color_hook_loop_tap.php">Color Hook & Loop Tape</a></li>
	</ul>
</li>
	<li id="menu-item-1501" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-1501"><a href="product-industry/pressure-sensitive-hook-and-loop/index.html">Adhesive Tap<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li id="menu-item-609" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-609"><a href="primium_adhesive_tapes.php">Premium Adhesive Tapes</a></li>
		<li id="menu-item-611" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-611"><a href="regular_adhesiv_tap.php">Regular Adhesive Tapes</a></li>
		<li id="menu-item-1499" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1499"><a href="adhesiv_piece_cuts.php">Adhesive Piece Cuts</a></li>
		<li id="menu-item-610" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-610"><a href="adhesiv_coin.php">Adhesive Coins</a></li>	
	</ul>
</li>
	<li id="menu-item-605" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-605"><a href="">Special Product<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li id="menu-item-623" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-623"><a href="inject_hook_moulded_hook.php">Inject Hook / Moulded Hook </a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-398" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-398"><a href="#">Applications<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
<<<<<<< HEAD
		<li id="menu-item-707" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-707"><a href="">Abrasives</a></li>

		<li id="menu-item-698" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-698"><a href="">Apparels</a></li>
	
		<li id="menu-item-702" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-702"><a href="">Aviation</a></li>
	
		<li id="menu-item-696" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-696"><a href="">Automotive</a></li>

		<li id="menu-item-700" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-700"><a href="">Cable Networking</a></li>

		<li id="menu-item-694" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-694"><a href="">Footwear</a></li>

		<li id="menu-item-704" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-704"><a href="">Hygiene</a></li>
	
		<li id="menu-item-706" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-706"><a href="">Luggage</a></li>

		<li id="menu-item-1348" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1348"><a href="">Medical Equipments</a></li>
	
		<li id="menu-item-1048" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1048"><a href="">PPE</a></li>

		<li id="menu-item-1050" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1050"><a href="">Stationery</a></li>
	
	<li id="menu-item-1045" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1045"><a href="">Orthopedic</a></li>
	<li id="menu-item-1046" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1046"><a href="">Defense</a></li>
	<li id="menu-item-1047" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1047"><a href="">Sports</a></li>
	<li id="menu-item-695" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-695"><a href="">Packaging</a></li>
	<li id="menu-item-699" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-699"><a href="">Infant Wear</a></li>
	<li id="menu-item-701" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-701"><a href="">FIBC</a></li>
	<li id="menu-item-705" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-705"><a href="">Home Furnishing</a></li>
	<li id="menu-item-708" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-708"><a href="">Media</a></li>
=======
	<li id="menu-item-694" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-694"><a href="">Abrasives</a></li>
	<li id="menu-item-1045" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1045"><a href="">Apparels</a></li>
	<li id="menu-item-1046" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1046"><a href="">Automotive</a></li>
	<li id="menu-item-1348" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1348"><a href="">Aviation</a></li>
	<li id="menu-item-696" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-696"><a href="">Cable Networking</a></li>
	<li id="menu-item-1047" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1047"><a href="">Defense</a></li>
	<li id="menu-item-1048" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1048"><a href="">FIBC</a></li>
	<li id="menu-item-695" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-695"><a href="">Footwear</a></li>
	<li id="menu-item-698" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-698"><a href="">Home Furnishing</a></li>
	<li id="menu-item-699" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-699"><a href="">Hygiene</a></li>
	<li id="menu-item-700" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-700"><a href="">Infant Wear</a></li>
	<li id="menu-item-701" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-701"><a href="">Luggage</a></li>
	<li id="menu-item-702" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-702"><a href="">Media</a></li>
	<li id="menu-item-704" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-704"><a href="">Medical Equipments</a></li>
	<li id="menu-item-705" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-705"><a href="">Orthopedic</a></li>
	<li id="menu-item-706" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-706"><a href="">Packaging</a></li>
	<li id="menu-item-707" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-707"><a href="">PPE</a></li>
	<li id="menu-item-1050" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1050"><a href="">Sports</a></li>
	<li id="menu-item-708" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-708"><a href="">Stationery</a></li>
>>>>>>> 88c47974ff5476a27aa7ed9ffbaacab51299dcec
	<li id="menu-item-1049" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1049"><a href="">Warehousing</a></li>
</ul>
</li>
<li id="menu-item-715" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-715"><a href="#">News<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
	<li id="menu-item-716" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-716"><a href="">Blog</a></li>
	<li id="menu-item-717" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-717"><a href="">Videos</a></li>
</ul>
</li>
<li id="menu-item-420" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-420"><a href="contact_us.php">Contact Us</a></li>
</ul>
</nav>

</div></div></div></div></header>