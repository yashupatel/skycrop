<?php

include('include/header.php');

?>
	<main id="main" class="site-main">

		
<section id="primary" class="content-area">
<div class="ct-container">
	<section>
		<nav class="woocommerce-breadcrumb"><a href="../../../index.html">Home</a>&nbsp;&#47;&nbsp;<a href="../../../product-industry/hook-and-loop-fasteners-for-sports-wear/index.html">Sports</a>&nbsp;&#47;&nbsp;Moulded/Injection Hook</nav>
		
			<div class="woocommerce-notices-wrapper"></div>
<div id="product-582" class="thumbs-left sticky-summary product type-product post-582 status-publish first instock product_cat-apparels product_cat-infant-wear product_cat-orthopedic product_cat-speciality-hook-and-loop product_cat-hook-and-loop-fasteners-for-sports-wear has-post-thumbnail featured shipping-taxable purchasable product-type-simple" data-structure="wide">

	<div class="product-entry-wrapper"><div class="ct-product-view">
	<div class="flexy-container" data-flexy="no">

		<div class="flexy">
			<div class="flexy-view" data-flexy-view="boxed">
				<div class="flexy-items ">
					<div><figure><a class="ct-image-container ct-lazy" href="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-600x900.jpg"><img width="600" height="900" class="attachment-woocommerce_single size-woocommerce_single" alt="Sky Micro Hook | Moulded Hook | Injection Hook | Baby Soft Hook | Black" sizes="(max-width: 600px) 100vw, 600px" data-lazy="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-600x900.jpg" data-lazy-set="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-600x900.jpg 600w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-200x300.jpg 200w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-683x1024.jpg 683w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-768x1152.jpg 768w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook.jpg 800w" data-object-fit="~"/><noscript><img width="600" height="900" src="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-600x900.jpg" class="attachment-woocommerce_single size-woocommerce_single" alt="Sky Micro Hook | Moulded Hook | Injection Hook | Baby Soft Hook | Black" srcset="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-600x900.jpg 600w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-200x300.jpg 200w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-683x1024.jpg 683w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-768x1152.jpg 768w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook.jpg 800w" sizes="(max-width: 600px) 100vw, 600px"/></noscript><div class="ct-ratio" style="padding-bottom: 133.3%"></div></a></figure></div><div><figure><a class="ct-image-container ct-lazy" href="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-600x813.jpg"><img width="600" height="813" class="attachment-woocommerce_single size-woocommerce_single" alt="Sky Micro Hook | Baby Soft Hook | Moulded Hook | Injection Hook | White" sizes="(max-width: 600px) 100vw, 600px" data-lazy="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-600x813.jpg" data-lazy-set="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-600x813.jpg 600w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-221x300.jpg 221w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-755x1024.jpg 755w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-768x1041.jpg 768w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White.jpg 900w" data-object-fit="~"/><noscript><img width="600" height="813" src="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-600x813.jpg" class="attachment-woocommerce_single size-woocommerce_single" alt="Sky Micro Hook | Baby Soft Hook | Moulded Hook | Injection Hook | White" srcset="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-600x813.jpg 600w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-221x300.jpg 221w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-755x1024.jpg 755w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-768x1041.jpg 768w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White.jpg 900w" sizes="(max-width: 600px) 100vw, 600px"/></noscript><div class="ct-ratio" style="padding-bottom: 133.3%"></div></a></figure></div><div><figure><a class="ct-image-container ct-lazy" href="https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-600x825.jpg"><img width="600" height="825" class="attachment-woocommerce_single size-woocommerce_single" alt="Sky Micro Hook | Baby Soft Hook | Moulded Hook | Injection Hook | Transparent" sizes="(max-width: 600px) 100vw, 600px" data-lazy="https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-600x825.jpg" data-lazy-set="https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-600x825.jpg 600w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-218x300.jpg 218w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-745x1024.jpg 745w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-768x1056.jpg 768w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent.jpg 800w" data-object-fit="~"/><noscript><img width="600" height="825" src="https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-600x825.jpg" class="attachment-woocommerce_single size-woocommerce_single" alt="Sky Micro Hook | Baby Soft Hook | Moulded Hook | Injection Hook | Transparent" srcset="https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-600x825.jpg 600w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-218x300.jpg 218w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-745x1024.jpg 745w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-768x1056.jpg 768w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent.jpg 800w" sizes="(max-width: 600px) 100vw, 600px"/></noscript><div class="ct-ratio" style="padding-bottom: 133.3%"></div></a></figure></div><div><figure><a class="ct-image-container ct-lazy" href="https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-600x825.jpg"><img width="600" height="825" class="attachment-woocommerce_single size-woocommerce_single" alt="Sky Micro Hook | Moulded Hook | Injection Hook | Baby Soft Hook | Black" sizes="(max-width: 600px) 100vw, 600px" data-lazy="https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-600x825.jpg" data-lazy-set="https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-600x825.jpg 600w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-218x300.jpg 218w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-745x1024.jpg 745w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-768x1056.jpg 768w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1.jpg 800w" data-object-fit="~"/><noscript><img width="600" height="825" src="https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-600x825.jpg" class="attachment-woocommerce_single size-woocommerce_single" alt="Sky Micro Hook | Moulded Hook | Injection Hook | Baby Soft Hook | Black" srcset="https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-600x825.jpg 600w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-218x300.jpg 218w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-745x1024.jpg 745w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-768x1056.jpg 768w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1.jpg 800w" sizes="(max-width: 600px) 100vw, 600px"/></noscript><div class="ct-ratio" style="padding-bottom: 133.3%"></div></a></figure></div>				</div>
			</div>
		</div>

		
	<nav class="flexy-pills" data-type="thumbs">
				<a class="ct-image-container active ct-lazy" href="#"><img width="300" height="300" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="Sky Micro Hook | Moulded Hook | Injection Hook | Baby Soft Hook | Black" sizes="(max-width: 300px) 100vw, 300px" data-lazy="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-300x300.jpg" data-lazy-set="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-300x300.jpg 300w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-100x100.jpg 100w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-150x150.jpg 150w" data-object-fit="~"/><noscript><img width="300" height="300" src="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="Sky Micro Hook | Moulded Hook | Injection Hook | Baby Soft Hook | Black" srcset="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-300x300.jpg 300w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-100x100.jpg 100w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px"/></noscript><div class="ct-ratio" style="padding-bottom: 100%"></div></a>		<a class="ct-image-container ct-lazy" href="#"><img width="300" height="300" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="Sky Micro Hook | Baby Soft Hook | Moulded Hook | Injection Hook | White" sizes="(max-width: 300px) 100vw, 300px" data-lazy="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-300x300.jpg" data-lazy-set="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-300x300.jpg 300w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-100x100.jpg 100w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-150x150.jpg 150w" data-object-fit="~"/><noscript><img width="300" height="300" src="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="Sky Micro Hook | Baby Soft Hook | Moulded Hook | Injection Hook | White" srcset="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-300x300.jpg 300w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-100x100.jpg 100w, https://www.skycorp.in/wp-content/uploads/2020/05/Sky-Micro-Hook-White-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px"/></noscript><div class="ct-ratio" style="padding-bottom: 100%"></div></a>		<a class="ct-image-container ct-lazy" href="#"><img width="300" height="300" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="Sky Micro Hook | Baby Soft Hook | Moulded Hook | Injection Hook | Transparent" sizes="(max-width: 300px) 100vw, 300px" data-lazy="https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-300x300.jpg" data-lazy-set="https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-300x300.jpg 300w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-150x150.jpg 150w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-100x100.jpg 100w" data-object-fit="~"/><noscript><img width="300" height="300" src="https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="Sky Micro Hook | Baby Soft Hook | Moulded Hook | Injection Hook | Transparent" srcset="https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-300x300.jpg 300w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-150x150.jpg 150w, https://www.skycorp.in/wp-content/uploads/2020/06/Sky-Micro-Hook-Transparent-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px"/></noscript><div class="ct-ratio" style="padding-bottom: 100%"></div></a>		<a class="ct-image-container ct-lazy" href="#"><img width="300" height="300" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="Sky Micro Hook | Moulded Hook | Injection Hook | Baby Soft Hook | Black" sizes="(max-width: 300px) 100vw, 300px" data-lazy="https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-300x300.jpg" data-lazy-set="https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-300x300.jpg 300w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-150x150.jpg 150w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-100x100.jpg 100w" data-object-fit="~"/><noscript><img width="300" height="300" src="https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="Sky Micro Hook | Moulded Hook | Injection Hook | Baby Soft Hook | Black" srcset="https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-300x300.jpg 300w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-150x150.jpg 150w, https://www.skycorp.in/wp-content/uploads/2020/05/Moulded-Hook-Black-2-1-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px"/></noscript><div class="ct-ratio" style="padding-bottom: 100%"></div></a>	</nav>

    	</div>
	</div>
	<div class="summary entry-summary">
		<h1 class="product_title entry-title">Moulded/Injection Hook</h1><p class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8377;</span>1.00</span></p>
<div class="woocommerce-product-details__short-description entry-content">
	<h1>Sky Micro Hook- Mouled/Injection Hook for Infant Wear</h1>
<p>Sky Micro Hook is one of the premium hook products. It is extremely soft in texture that prevents any injury or rashes to baby skin. It is a skin-friendly product barring any allergic reactions to baby skin. It is utilized for closure in many Infant Wear Products, and Accessories like in Diapers, Nappies, and Bibs. It possesses material that ensures proper closure and safety to the baby.</p>
<p>It is highly recommended for the infant wear, it is also used in apparel and some specific automotive and ancillary applications.</p>
<p>It is mainly utilized in high strength applications.</p>
<p>&nbsp;</p>
</div>

	
	<form class="cart" action="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-micro-hook-moulded-injection-hook/" method="post" enctype='multipart/form-data'>
		
		<div class="ct-cart-actions">	<div class="quantity">
				<label class="screen-reader-text" for="quantity_5f000f441a4bd">Moulded/Injection Hook quantity</label>
		<input type="number" id="quantity_5f000f441a4bd" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" placeholder="" inputmode="numeric"/>
		<span class="ct-increase"></span><span class="ct-decrease"></span>	</div>
	
		<button type="submit" name="add-to-cart" value="582" class="single_add_to_cart_button button alt">Add to cart</button>

		</div>	</form>

	
<div class="product_meta">

	
	
	<span class="posted_in">Categories: <a href="../../../product-industry/apparels/index.html" rel="tag">Apparels</a>, <a href="../../../product-industry/infant-wear/index.html" rel="tag">Infant Wear</a>, <a href="../../../product-industry/orthopedic/index.html" rel="tag">Orthopedic</a>, <a href="../../../product-industry/speciality-hook-and-loop/index.html" rel="tag">Speciality Products</a>, <a href="../../../product-industry/hook-and-loop-fasteners-for-sports-wear/index.html" rel="tag">Sports</a></span>
	
	
</div>
	</div>

	</div>

	
	<div class="woocommerce-tabs wc-tabs-wrapper">
		<ul class="tabs wc-tabs" role="tablist">
							<li class="description_tab" id="tab-title-description" role="tab" aria-controls="tab-description">
					<a href="#tab-description">
						Description					</a>
				</li>
							<li class="reviews_tab" id="tab-title-reviews" role="tab" aria-controls="tab-reviews">
					<a href="#tab-reviews">
						Reviews (0)					</a>
				</li>
					</ul>
					<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="tab-description" role="tabpanel" aria-labelledby="tab-title-description">
				
	<h2>Description</h2>

<p><strong>AVAILABILITY: </strong><br/>
• Standard Colours – Black, White and Transparent</p>
			</div>
					<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--reviews panel entry-content wc-tab" id="tab-reviews" role="tabpanel" aria-labelledby="tab-title-reviews">
				<div id="reviews" class="woocommerce-Reviews">
	<div id="comments">
		<h2 class="woocommerce-Reviews-title">
			Reviews		</h2>

					<p class="woocommerce-noreviews">There are no reviews yet.</p>
			</div>

			<div id="review_form_wrapper">
			<div id="review_form">
					<div id="respond" class="comment-respond">
		<span id="reply-title" class="comment-reply-title">Be the first to review &ldquo;Moulded/Injection Hook&rdquo; <small><a rel="nofollow" id="cancel-comment-reply-link" href="index.html#respond" style="display:none;">Cancel reply</a></small></span><form action="https://www.skycorp.in/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate><p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p><p class="comment-form-author"><input id="author" name="author" type="text" value="" size="30" required /><label for="author">Name&nbsp;<span class="required">*</span></label></p>
<p class="comment-form-email"> <input id="email" name="email" type="email" value="" size="30" required /><label for="email">Email&nbsp;<span class="required">*</span></label></p>
<p class="comment-form-cookies-consent"><input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes"/> <label for="wp-comment-cookies-consent">Save my name, email, and website in this browser for the next time I comment.</label></p>
<div class="comment-form-rating"><label for="rating">Your rating</label><select name="rating" id="rating" required>
			<option value="">Rate&hellip;</option>
			<option value="5">Perfect</option>
			<option value="4">Good</option>
			<option value="3">Average</option>
			<option value="2">Not that bad</option>
			<option value="1">Very poor</option>
		</select></div><p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" required></textarea><label for="comment">Your review&nbsp;<span class="required">*</span></label></p><p class="form-submit"><button name="submit" type="submit" id="submit" class="submit woo-review-submit" value="Submit"/>Submit</button> <input type='hidden' name='comment_post_ID' value='582' id='comment_post_ID'/>
<input type='hidden' name='comment_parent' id='comment_parent' value='0'/>
</p></form>	</div><!-- #respond -->
				</div>
		</div>
	
	<div class="clear"></div>
</div>
			</div>
		
			</div>

</div>



		
		
	</section>

	</div>
</section>


				
	</main>

	<footer class="site-footer"><div data-row="top"><div class="ct-container-fluid" data-stack="tablet"><div data-column="socials">
<div class="ct-footer-socials ">

	
		<div class="ct-social-box" data-type="simple" data-size="custom" data-color="custom">
			
							
				<a href="https://www.facebook.com/hookandloopmanufacturer" target="_blank" data-network="facebook">
					
				<svg width="20px" height="20px" viewBox="0 0 20 20">
					<path d="M20,10.1c0-5.5-4.5-10-10-10S0,4.5,0,10.1c0,5,3.7,9.1,8.4,9.9v-7H5.9v-2.9h2.5V7.9C8.4,5.4,9.9,4,12.2,4c1.1,0,2.2,0.2,2.2,0.2v2.5h-1.3c-1.2,0-1.6,0.8-1.6,1.6v1.9h2.8L13.9,13h-2.3v7C16.3,19.2,20,15.1,20,10.1z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						Facebook					</span>
				</a>
							
				<a href="https://www.youtube.com/channel/UCkzb_pQ2qqOpZjZMjUyrjag" target="_blank" data-network="youtube">
					
				<svg width="20" height="20" viewbox="0 0 20 20">
					<path d="M15,0H5C2.2,0,0,2.2,0,5v10c0,2.8,2.2,5,5,5h10c2.8,0,5-2.2,5-5V5C20,2.2,17.8,0,15,0z M14.5,10.9l-6.8,3.8c-0.1,0.1-0.3,0.1-0.5,0.1c-0.5,0-1-0.4-1-1l0,0V6.2c0-0.5,0.4-1,1-1c0.2,0,0.3,0,0.5,0.1l6.8,3.8c0.5,0.3,0.7,0.8,0.4,1.3C14.8,10.6,14.6,10.8,14.5,10.9z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						YouTube					</span>
				</a>
							
				<a href="https://www.linkedin.com/company/sky-industries-ltd" target="_blank" data-network="linkedin">
					
				<svg width="20px" height="20px" viewBox="0 0 20 20">
					<path d="M18.6,0H1.4C0.6,0,0,0.6,0,1.4v17.1C0,19.4,0.6,20,1.4,20h17.1c0.8,0,1.4-0.6,1.4-1.4V1.4C20,0.6,19.4,0,18.6,0z M6,17.1h-3V7.6h3L6,17.1L6,17.1zM4.6,6.3c-1,0-1.7-0.8-1.7-1.7s0.8-1.7,1.7-1.7c0.9,0,1.7,0.8,1.7,1.7C6.3,5.5,5.5,6.3,4.6,6.3z M17.2,17.1h-3v-4.6c0-1.1,0-2.5-1.5-2.5c-1.5,0-1.8,1.2-1.8,2.5v4.7h-3V7.6h2.8v1.3h0c0.4-0.8,1.4-1.5,2.8-1.5c3,0,3.6,2,3.6,4.5V17.1z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						LinkedIn					</span>
				</a>
			
					</div>

	</div>

</div><div data-column="widget-area-1"><div class="widget_text ct-widget widget_custom_html"><div class="textwidget entry-content custom-html-widget"><!-- Begin Mailchimp Signup Form -->
<link href="http://cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
	/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="https://skycorp.us8.list-manage.com/subscribe/post?u=d7b761504b138559ff5198267&amp;id=9683e1b32c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Subscribe</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d7b761504b138559ff5198267_9683e1b32c" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup--></div></div></div></div></div><div data-row="middle"><div class="ct-container-fluid"><div data-column="menu">
<nav id="footer-menu" class="footer-menu">

	<ul id="menu-footer-menu" class="menu"><li id="menu-item-366" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-366"><a href="../../../hook-and-loop-online/index.html">Shop</a></li>
<li id="menu-item-378" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-378"><a href="../../../hook-and-loop-tape-manufacturer-contact-us/index.html">Contact Us</a></li>
<li id="menu-item-364" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-364"><a href="../../../hook-and-loop-in-india-about-us/index.html">About Us</a></li>
<li id="menu-item-365" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-365"><a href="../../../my-account/index.html">My account</a></li>
<li id="menu-item-1296" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1296"><a href="../../../terms-and-conditions/index.html">Terms And Conditions</a></li>
</ul>
</nav>
</div></div></div><div data-row="bottom"><div class="ct-container-fluid"><div data-column="copyright">
<div class="ct-footer-copyright">

	<p>Copyright © 2020 Skycorp - Developed By Amit</p></div>
</div></div></div></footer></div>


		<div id="search-modal" class="ct-panel" data-behaviour="modal">
			<div class="close-button">
				<span class="ct-trigger closed">
					<span></span>
				</span>
			</div>

			<div class="content-container" data-align="middle">
				

<form role="search" method="get" class="search-form" action="https://www.skycorp.in/" data-live-results>
	<div class="ct-search-input">
		<input type="search" class="modal-field" placeholder="Search" value="" name="s" autocomplete="off" title="Search Input"/>

		<button class="search-submit">
			<span hidden>Search</span>
			<i>
				<svg width="12px" height="12px" viewBox="0 0 24 24">

					<path d="M23.3,23.3c-0.9,0.9-2.3,0.9-3.2,0l-4-4c-1.6,1-3.6,1.7-5.7,1.7C4.7,21,0,16.3,0,10.5S4.7,0,10.5,0C16.3,0,21,4.7,21,10.5
					c0,2.1-0.6,4-1.7,5.7l4,4C24.2,21,24.2,22.5,23.3,23.3z M10.5,3C6.4,3,3,6.4,3,10.5S6.4,18,10.5,18c4.1,0,7.5-3.4,7.5-7.5
					C18,6.4,14.7,3,10.5,3z"/>
				</svg>
			</i>

			<span data-loader="circles"><span></span><span></span><span></span></span>
		</button>

		
		

	</div>
</form>


			</div>
		</div>

		
		<div id="account-modal" class="ct-panel" data-behaviour="modal">
			<div class="close-button">
				<span class="ct-trigger closed">
					<span></span>
				</span>
			</div>

			<div class="content-container" data-align="middle">
				
		<form name="loginform" id="loginform" action="https://www.skycorp.in/wp-login.php" method="post">
			
			<p class="login-username">
				<label for="user_login">Username or Email Address</label>
				<input type="text" name="log" id="user_login" class="input" value="" size="20"/>
			</p>
			<p class="login-password">
				<label for="user_pass">Password</label>
				<input type="password" name="pwd" id="user_pass" class="input" value="" size="20"/>
			</p>
			
			<p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever"/> Remember Me</label></p>
			<p class="login-submit">
				<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Log In"/>
				<input type="hidden" name="redirect_to" value="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-micro-hook-moulded-injection-hook/"/>
			</p>
			
		</form>			</div>
		</div>

		
<a class="skip-link screen-reader-text" href="#primary">
Skip to content</a>

	

	<div class="cookie-notification ct-fade-in-start" data-period="forever" data-type="type-1">

		<div class="container">
							<div class="ct-cookies-content">We use cookies to ensure that we give you the best experience on our website.</div>
			
			<button type="submit" class="ct-accept">Accept</button>

							<button class="ct-close">×</button>
			
		</div>
	</div>
	
<div id="offcanvas" class="ct-panel" data-behaviour="left-side" data-device="mobile"><div class="close-button">
				<span class="ct-trigger closed">
					<span></span>
				</span>
			</div><div class="content-container"><section data-align="left">
<nav class="mobile-menu has-submenu" data-id="mobile-menu" data-type="type-1">
	<ul id="menu-main-menu-1" class=""><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-369"><a href="../../../index.html">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children menu-item-243"><a href="#">Products<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-418"><a href="#">Hook and Loop<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1498"><a href="../../hook-and-loop-fasteners/sky-magic-hs-hook-and-loop-fastener/index.html">High Strength</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-600"><a href="../../hook-and-loop-fasteners/sky-magic-hg-hook-and-loop/index.html">High Grip</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-601"><a href="../../hook-and-loop-fasteners/sky-magic-fire-retardant-hook-and-loop/index.html">Fire Retardant</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1257"><a href="../../hook-and-loop-fasteners/sky-walker-sew-on-hook-and-loop/index.html">Nylon x Poly Mix</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1258"><a href="../../hook-and-loop-fasteners/anti-bacterial-hook-and-loop/index.html">Antibacterial Hook and Loop</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-630"><a href="../../hook-and-loop-fasteners/sky-soft-hook-fastener/index.html">Soft Hook</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-628"><a href="../../hook-and-loop-fasteners/sky-unnapped-loop-fastener/index.html">Unnapped Loop</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-629"><a href="../../hook-and-loop-fasteners/sky-mushroom-hook-fastening-tape/index.html">Mushroom Hook</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1293"><a href="../../hook-and-loop-fasteners/sky-printee-printed-hook-loop/index.html">Printed Hook/Loop</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1496"><a href="../../hook-and-loop-fasteners/sky-hook-and-loop-piece-cuts/index.html">Piece Cuts</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1295"><a href="../../hook-and-loop-fasteners/customized-hook-and-loop-shapes/index.html">Customized Shapes</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-1501"><a href="../../../product-industry/pressure-sensitive-hook-and-loop/index.html">Pressure Sensitive Hook and Loop<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-609"><a href="../../pressure-sensitive-hook-and-loop/premium-rubber-based-adhesive-tapes/index.html">Premium Rubber Based Adhesive Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-611"><a href="../../pressure-sensitive-hook-and-loop/sky-wat-regular-pressure-sensitive-hook-and-loop/index.html">Regular Rubber Based Adhesive Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-610"><a href="../../pressure-sensitive-hook-and-loop/sky-ezee-adhesive-hook-and-loop-coins/index.html">Adhesive Coins</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1644"><a href="../../pressure-sensitive-hook-and-loop/adhesive-back-to-back-tape/index.html">Adhesive Back to Back Tape</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-612"><a href="../../pressure-sensitive-hook-and-loop/sky-score-cuts-adhesive-hook-and-loop-cuts/index.html">Adhesive Score Cuts</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1499"><a href="../../pressure-sensitive-hook-and-loop/sky-piece-cuts-adhesive-hook-and-loop-pieces/index.html">Adhesive Piece Cuts</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1260"><a href="../../pressure-sensitive-hook-and-loop/sky-buddies-hook-and-loop-mated-piece-cuts/index.html">Adhesive Mated Piece Cuts</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-ancestor current-menu-parent current-product-parent menu-item-has-children menu-item-605"><a href="../../../product-industry/speciality-hook-and-loop/index.html">Speciality Products<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product current-menu-item menu-item-623"><a href="index.html" aria-current="page">Moulded/Injection Hook</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-624"><a href="../sky-slim-knit-loop/index.html">Knit Loop (Velour)</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1262"><a href="../sky-hook-and-loop-back-to-back/index.html">Back to Back Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1263"><a href="../sky-laminated-hook-and-loop-fastener/index.html">Laminated Range</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-626"><a href="../sky-c-fold-loop/index.html">C-Fold Loop</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-627"><a href="../sky-hook-and-loop-mixed-tape/index.html">Hook-Loop Mix Tape</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-631"><a href="../sky-stretchi-elasticated-loop-tapes/index.html">Elasticated Loop Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-632"><a href="../sky-bobbin-hook-and-loop-tapes/index.html">Bobbin Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1264"><a href="../sky-weldable-hook-and-loop-fastener/index.html">Weldable Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-625"><a href="../sky-hook-loop-side-by-side/index.html">Side by Side Hook-Loop</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1283"><a href="#">Customized Range<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1278"><a href="../../customized-hook-and-loop/headgear-for-respiratory-system/index.html">Headgear for Respiratory System</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1279"><a href="../../customized-hook-and-loop/sky-auto-sleeve-hook-and-loop-closure/index.html">Auto Sleeve</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1281"><a href="../../customized-hook-and-loop/hook-and-loop-cuff-sleeve/index.html">Cuff Sleeve</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1361"><a href="../../customized-hook-and-loop/hook-and-loop-packaging-for-corrugated-boxes/index.html">Packaging for Corrugated Boxes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1277"><a href="../../customized-hook-and-loop/tile-grip-adhesive-hook-dots/index.html">Tile Grip</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-1268"><a href="../../../product-industry/hook-and-loop-straps/index.html">Hook and Loop Straps<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1273"><a href="../../hook-and-loop-straps/cable-ties-hook-and-loop-ties/index.html">Cable Ties</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1269"><a href="../../hook-and-loop-straps/sky-luggage-strap/index.html">Luggage Strap</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1270"><a href="../../hook-and-loop-straps/pallet-strap-hook-and-loop-strap/index.html">Pallet Strap</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1271"><a href="../../hook-and-loop-straps/sky-tourniquet-band/index.html">Tourniquet</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1272"><a href="../../hook-and-loop-straps/sky-file-strap/index.html">File Strap</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1274"><a href="../../hook-and-loop-straps/customized-straps/index.html">Customized Straps</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-1502"><a href="../../../product-industry/aplix-fastening-solutions/index.html">Aplix Products<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-635"><a href="../../aplix-fastening-solutions/aplix-automotive-fastening-solutions/index.html">Automotive</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-637"><a href="../../aplix-fastening-solutions/aplix-aerospace-products/index.html">Aircraft</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-639"><a href="../../aplix-fastening-solutions/aplix-hygiene-fastening-solution/index.html">Hygiene</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-640"><a href="../../aplix-fastening-solutions/aplix-packaging-fastening-solution/index.html">Packaging</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1347"><a href="#">Other Products<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-1282"><a href="../../../product-industry/webbings-tapes/index.html">Webbings<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
		<ul class="sub-menu">
			<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1275"><a href="../../webbings-tapes/nylon-webbings/index.html">Nylon Webbings</a></li>
			<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1276"><a href="../../webbings-tapes/polyster-webbings/index.html">Polyester Webbings</a></li>
		</ul>
</li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-621"><a href="../../sky-other-products/sky-velvet-tapes-ribbons/index.html">Velvet Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-620"><a href="../../sky-other-products/sky-fiber-glass-insect-screen/index.html">Insect Screen</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-622"><a href="../../sky-other-products/elastics/index.html">Elastics</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-633"><a href="../../sky-other-products/neoprene-fabric/index.html">Neoprene Fabrics</a></li>
	</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-398"><a href="#">Industry/Applications<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-694"><a href="../../../product-industry/footwear/index.html">Footwear</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-parent current-product-parent menu-item-1045"><a href="../../../product-industry/orthopedic/index.html">Orthopedic</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1046"><a href="../../../product-industry/defense-and-military-wear/index.html">Defense</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1348"><a href="../../../product-industry/medical-equipments/index.html">Medical Equipments</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-696"><a href="../../../product-industry/automotive/index.html">Automotive</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-parent current-product-parent menu-item-1047"><a href="../../../product-industry/hook-and-loop-fasteners-for-sports-wear/index.html">Sports</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1048"><a href="../../../product-industry/ppe/index.html">PPE</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-695"><a href="../../../product-industry/packaging/index.html">Packaging</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-parent current-product-parent menu-item-698"><a href="../../../product-industry/apparels/index.html">Apparels</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-parent current-product-parent menu-item-699"><a href="../../../product-industry/infant-wear/index.html">Infant Wear</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-700"><a href="../../../product-industry/cable-networking/index.html">Cable Networking</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-701"><a href="../../../product-industry/fibc/index.html">FIBC</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-702"><a href="../../../product-industry/aviation/index.html">Aviation</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-704"><a href="../../../product-industry/hygiene-and-baby-care/index.html">Hygiene</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-705"><a href="../../../product-industry/home-furnishing/index.html">Home Furnishing</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-706"><a href="../../../product-industry/hook-and-loop-strap-for-luggage/index.html">Luggage</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-707"><a href="../../../product-industry/abrasives/index.html">Abrasives</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1050"><a href="../../../product-industry/stationery/index.html">Stationery</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-708"><a href="../../../product-industry/media/index.html">Media</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1049"><a href="../../../product-industry/warehousing/index.html">Warehousing</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-397"><a href="../../../investor-relation/index.html">Investor Relation<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1051"><a href="../../../board-of-directors/index.html">Board of Directors</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1053"><a href="../../../appointment-of-independent-directors/index.html">Terms Of Appointment Of Independent Directors</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1056"><a href="#">Financial Reports<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1052"><a href="../../../annual-reports/index.html">Annual Reports</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1062"><a href="../../../annual-reports-2/index.html">Quarterly Reports</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1064"><a href="../../../mgt-9-details-for-investors/index.html">MGT-9</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1055"><a href="#">Policies and Code of Conduct<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1101"><a href="../../../sky-policies-adopted-2/index.html">Sky Policies Adopted</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1057"><a href="#">Shareholder Information<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1067"><a href="../../../notice-avt/index.html">Notice &#038; Avt</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1082"><a href="../../../results/index.html">Results</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1071"><a href="../../../investor-contacts/index.html">Investor Contacts</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1073"><a href="../../../iepf/index.html">IEPF</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1076"><a href="../../../notice/index.html">Notice</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1079"><a href="../../../corporate-annoucements/index.html">Corporate Annoucements</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-715"><a href="#">News<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-716"><a href="../../../hook-and-loop-blogs/index.html">Blog</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-717"><a href="../../../hook-and-loop-uses-videos/index.html">Videos</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1201"><a href="../../../hook-and-loop-online/index.html">Shop</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-420"><a href="../../../hook-and-loop-tape-manufacturer-contact-us/index.html">Contact Us</a></li>
</ul></nav>

<div class="ct-header-socials " data-id="socials">

	
		<div class="ct-social-box" data-type="simple" data-size="custom" data-color="custom">
			
							
				<a href="https://www.facebook.com/hookandloopmanufacturer" target="_blank" data-network="facebook">
					
				<svg width="20px" height="20px" viewBox="0 0 20 20">
					<path d="M20,10.1c0-5.5-4.5-10-10-10S0,4.5,0,10.1c0,5,3.7,9.1,8.4,9.9v-7H5.9v-2.9h2.5V7.9C8.4,5.4,9.9,4,12.2,4c1.1,0,2.2,0.2,2.2,0.2v2.5h-1.3c-1.2,0-1.6,0.8-1.6,1.6v1.9h2.8L13.9,13h-2.3v7C16.3,19.2,20,15.1,20,10.1z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						Facebook					</span>
				</a>
							
				<a href="https://www.linkedin.com/company/sky-industries-ltd" target="_blank" data-network="linkedin">
					
				<svg width="20px" height="20px" viewBox="0 0 20 20">
					<path d="M18.6,0H1.4C0.6,0,0,0.6,0,1.4v17.1C0,19.4,0.6,20,1.4,20h17.1c0.8,0,1.4-0.6,1.4-1.4V1.4C20,0.6,19.4,0,18.6,0z M6,17.1h-3V7.6h3L6,17.1L6,17.1zM4.6,6.3c-1,0-1.7-0.8-1.7-1.7s0.8-1.7,1.7-1.7c0.9,0,1.7,0.8,1.7,1.7C6.3,5.5,5.5,6.3,4.6,6.3z M17.2,17.1h-3v-4.6c0-1.1,0-2.5-1.5-2.5c-1.5,0-1.8,1.2-1.8,2.5v4.7h-3V7.6h2.8v1.3h0c0.4-0.8,1.4-1.5,2.8-1.5c3,0,3.6,2,3.6,4.5V17.1z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						LinkedIn					</span>
				</a>
							
				<a href="https://www.youtube.com/channel/UCkzb_pQ2qqOpZjZMjUyrjag" target="_blank" data-network="youtube">
					
				<svg width="20" height="20" viewbox="0 0 20 20">
					<path d="M15,0H5C2.2,0,0,2.2,0,5v10c0,2.8,2.2,5,5,5h10c2.8,0,5-2.2,5-5V5C20,2.2,17.8,0,15,0z M14.5,10.9l-6.8,3.8c-0.1,0.1-0.3,0.1-0.5,0.1c-0.5,0-1-0.4-1-1l0,0V6.2c0-0.5,0.4-1,1-1c0.2,0,0.3,0,0.5,0.1l6.8,3.8c0.5,0.3,0.7,0.8,0.4,1.3C14.8,10.6,14.6,10.8,14.5,10.9z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						YouTube					</span>
				</a>
			
					</div>

	
</div>
</section></div></div>

<script>jQuery('li#tab-title-description a').text('Product Details');jQuery(window).load(function(){jQuery('.ct-cart-actions').before('<div class="ct-header-cta" id="quotation_button" data-id="button"><a href="/request-a-quote/" class="ct-button" data-size="medium" target="_blank">Request A Quote</a></div>');jQuery('#quotation_button').hide();});jQuery('select#type').on('change',function(){var selectVal=jQuery("#type option:selected").val();if(selectVal=="Custom Dimensions"){jQuery('.ct-cart-actions').hide();jQuery('#quotation_button').show();}else{jQuery('#quotation_button').hide();jQuery('.ct-cart-actions').show();}});if(window.location.pathname=="/product/aplix-packaging-fastening-solution/"){jQuery('form.variations_form.cart').show();}</script>
<script id="ct-header-template-desktop" type="text-template/desktop"><![CDATA[<header id="header" data-header="static" data-device="desktop" ><div data-row="middle" data-columns="3" ><div class="ct-container" ><div data-column="start" data-placements="1" ><div data-items="primary" >
<div
	class="ct-header-text "
	data-id="text" >
	<div class="entry-content">
		<p><a class="btn-tel" href="tel:8775321561"><img class="alignnone  wp-image-486" src="https://35.203.161.219/wp-content/uploads/2020/05/ico02.png" alt="" width="21" height="21" /> +919870959550 </a></p>	</div>
</div>

<div
	class="ct-header-cta "
	data-id="button" >

	<a
		href="/request-a-quote/"
		class="ct-button"
		data-size="medium"
		target="_blank">
		Request A Quote	</a>
</div>
</div></div><div data-column="middle" ><div data-items="" >
<div
	class="site-branding"
	data-id="logo" 	>

	<h1 class="site-title" >
					<a href="https://www.skycorp.in/" class="custom-logo-link" rel="home"><img width="2598" height="1181" src="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-logo-2.png" class="custom-logo" alt="Skycorp" /></a>			</h1>

	</div>

</div></div><div data-column="end" data-placements="1" ><div data-items="primary" >
<div
	class="ct-header-search "
	data-id="search" >

	<a href="#search-modal">
		<svg viewBox="0 0 10 10">
			<path d="M9.7,9.7c-0.4,0.4-1,0.4-1.3,0L6.7,8.1C6.1,8.5,5.3,8.8,4.4,8.8C2,8.8,0,6.8,0,4.4S2,0,4.4,0s4.4,2,4.4,4.4
		c0,0.9-0.3,1.7-0.7,2.4l1.7,1.7C10.1,8.8,10.1,9.4,9.7,9.7z M4.4,1.3c-1.7,0-3.1,1.4-3.1,3.1s1.4,3.1,3.1,3.1c1.7,0,3.1-1.4,3.1-3.1
		S6.1,1.3,4.4,1.3z"/>
		</svg>
		<span hidden>Search</span>
	</a>
</div>

<div
	class="ct-header-account"
	data-id="account" >

	
			<a href="#account-modal">
			<svg width="15" height="15" viewBox="0 0 20 20">
				<path d="M10 0C4.5 0 0 4.5 0 10s4.5 10 10 10 10-4.5 10-10S15.5 0 10 0zm0 2c4.4 0 8 3.6 8 8 0 1.6-.5 3.1-1.3 4.3l-.8-.6C14.4 12.5 11.5 12 10 12s-4.4.5-5.9 1.7l-.8.6C2.5 13.1 2 11.6 2 10c0-4.4 3.6-8 8-8zm0 1.8C8.2 3.8 6.8 5.2 6.8 7c0 1.8 1.5 3.3 3.2 3.3s3.2-1.5 3.2-3.3c0-1.8-1.4-3.2-3.2-3.2zm0 1.9c.7 0 1.2.6 1.2 1.2 0 .7-.6 1.2-1.2 1.2S8.8 7.7 8.8 7c0-.7.5-1.3 1.2-1.3zm0 8.3c3.1 0 4.8 1.2 5.5 1.8-1.4 1.3-3.3 2.2-5.5 2.2s-4.1-.9-5.5-2.2c.7-.6 2.4-1.8 5.5-1.8zm-5.9 1.3c.1.1.2.3.4.4-.2-.1-.3-.2-.4-.4zm11.8.1c-.1.1-.2.2-.3.4.1-.2.2-.3.3-.4z"/>
			</svg>

					</a>
	</div>


<div
	class="ct-header-cart "
			data-id="cart" >

	<a class="ct-cart-item"
		href="https://www.skycorp.in/cart/"
				data-count="0">

		<span class="ct-cart-label">Cart</span>
		
		<i>
			<svg viewBox="0 0 10 10"><path d="M10,8.9L9.6,1c0-0.6-0.4-1-1.1-1H1.4c-0.6,0-1,0.4-1,1L0,8.9l0,0C0,9.6,0.4,10,1,10h7.9C9.6,10,10,9.6,10,8.9L10,8.9zM8.9,9.1H1C0.9,9.1,0.9,9.1,0.9,9L1.2,1l0,0c0-0.1,0-0.1,0.1-0.1h7.2c0.1,0,0.1,0.1,0.1,0.1l0,0l0.4,7.9C9.1,9.1,9.1,9.1,8.9,9.1zM6.5,1.8C6.2,1.8,6.1,2,6.1,2.2v1.3c0,0.6-0.4,1.1-1.1,1.1c-0.6,0-1-0.5-1-1.1V2.2c0-0.2-0.2-0.5-0.5-0.5S3,1.9,3,2.2v1.3c0,1.1,0.9,1.9,1.9,1.9s1.9-0.8,1.9-1.9V2.2C6.9,2,6.8,1.8,6.5,1.8z"/></svg>		</i>
	</a>
</div>
</div></div></div></div><div data-row="bottom" data-columns="1" data-border="top-fixed" ><div class="ct-container" ><div data-column="middle" ><div data-items="" >
<nav
	id="header-menu-1"
	class="header-menu-1"
	data-id="menu" 	data-type="type-1"
	data-dropdown-animation="type-3"
		data-responsive	>

	<ul id="menu-main-menu-2" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-369"><a href="https://www.skycorp.in/">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children menu-item-243"><a href="#">Products<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-418"><a href="#">Hook and Loop<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1498"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/sky-magic-hs-hook-and-loop-fastener/">High Strength</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-600"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/sky-magic-hg-hook-and-loop/">High Grip</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-601"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/sky-magic-fire-retardant-hook-and-loop/">Fire Retardant</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1257"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/sky-walker-sew-on-hook-and-loop/">Nylon x Poly Mix</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1258"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/anti-bacterial-hook-and-loop/">Antibacterial Hook and Loop</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-630"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/sky-soft-hook-fastener/">Soft Hook</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-628"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/sky-unnapped-loop-fastener/">Unnapped Loop</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-629"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/sky-mushroom-hook-fastening-tape/">Mushroom Hook</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1293"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/sky-printee-printed-hook-loop/">Printed Hook/Loop</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1496"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/sky-hook-and-loop-piece-cuts/">Piece Cuts</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1295"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-fasteners/customized-hook-and-loop-shapes/">Customized Shapes</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-1501"><a href="https://www.skycorp.in/product-industry/pressure-sensitive-hook-and-loop/">Pressure Sensitive Hook and Loop<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-609"><a href="https://www.skycorp.in/hook-and-loop-all-products/pressure-sensitive-hook-and-loop/premium-rubber-based-adhesive-tapes/">Premium Rubber Based Adhesive Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-611"><a href="https://www.skycorp.in/hook-and-loop-all-products/pressure-sensitive-hook-and-loop/sky-wat-regular-pressure-sensitive-hook-and-loop/">Regular Rubber Based Adhesive Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-610"><a href="https://www.skycorp.in/hook-and-loop-all-products/pressure-sensitive-hook-and-loop/sky-ezee-adhesive-hook-and-loop-coins/">Adhesive Coins</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1644"><a href="https://www.skycorp.in/hook-and-loop-all-products/pressure-sensitive-hook-and-loop/adhesive-back-to-back-tape/">Adhesive Back to Back Tape</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-612"><a href="https://www.skycorp.in/hook-and-loop-all-products/pressure-sensitive-hook-and-loop/sky-score-cuts-adhesive-hook-and-loop-cuts/">Adhesive Score Cuts</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1499"><a href="https://www.skycorp.in/hook-and-loop-all-products/pressure-sensitive-hook-and-loop/sky-piece-cuts-adhesive-hook-and-loop-pieces/">Adhesive Piece Cuts</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1260"><a href="https://www.skycorp.in/hook-and-loop-all-products/pressure-sensitive-hook-and-loop/sky-buddies-hook-and-loop-mated-piece-cuts/">Adhesive Mated Piece Cuts</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-ancestor current-menu-parent current-product-parent menu-item-has-children menu-item-605"><a href="https://www.skycorp.in/product-industry/speciality-hook-and-loop/">Speciality Products<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product current-menu-item menu-item-623"><a href="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-micro-hook-moulded-injection-hook/" aria-current="page">Moulded/Injection Hook</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-624"><a href="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-slim-knit-loop/">Knit Loop (Velour)</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1262"><a href="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-hook-and-loop-back-to-back/">Back to Back Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1263"><a href="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-laminated-hook-and-loop-fastener/">Laminated Range</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-626"><a href="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-c-fold-loop/">C-Fold Loop</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-627"><a href="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-hook-and-loop-mixed-tape/">Hook-Loop Mix Tape</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-631"><a href="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-stretchi-elasticated-loop-tapes/">Elasticated Loop Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-632"><a href="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-bobbin-hook-and-loop-tapes/">Bobbin Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1264"><a href="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-weldable-hook-and-loop-fastener/">Weldable Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-625"><a href="https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-hook-loop-side-by-side/">Side by Side Hook-Loop</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1283"><a href="#">Customized Range<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1278"><a href="https://www.skycorp.in/hook-and-loop-all-products/customized-hook-and-loop/headgear-for-respiratory-system/">Headgear for Respiratory System</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1279"><a href="https://www.skycorp.in/hook-and-loop-all-products/customized-hook-and-loop/sky-auto-sleeve-hook-and-loop-closure/">Auto Sleeve</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1281"><a href="https://www.skycorp.in/hook-and-loop-all-products/customized-hook-and-loop/hook-and-loop-cuff-sleeve/">Cuff Sleeve</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1361"><a href="https://www.skycorp.in/hook-and-loop-all-products/customized-hook-and-loop/hook-and-loop-packaging-for-corrugated-boxes/">Packaging for Corrugated Boxes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1277"><a href="https://www.skycorp.in/hook-and-loop-all-products/customized-hook-and-loop/tile-grip-adhesive-hook-dots/">Tile Grip</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-1268"><a href="https://www.skycorp.in/product-industry/hook-and-loop-straps/">Hook and Loop Straps<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1273"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-straps/cable-ties-hook-and-loop-ties/">Cable Ties</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1269"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-straps/sky-luggage-strap/">Luggage Strap</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1270"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-straps/pallet-strap-hook-and-loop-strap/">Pallet Strap</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1271"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-straps/sky-tourniquet-band/">Tourniquet</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1272"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-straps/sky-file-strap/">File Strap</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1274"><a href="https://www.skycorp.in/hook-and-loop-all-products/hook-and-loop-straps/customized-straps/">Customized Straps</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-1502"><a href="https://www.skycorp.in/product-industry/aplix-fastening-solutions/">Aplix Products<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-635"><a href="https://www.skycorp.in/hook-and-loop-all-products/aplix-fastening-solutions/aplix-automotive-fastening-solutions/">Automotive</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-637"><a href="https://www.skycorp.in/hook-and-loop-all-products/aplix-fastening-solutions/aplix-aerospace-products/">Aircraft</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-639"><a href="https://www.skycorp.in/hook-and-loop-all-products/aplix-fastening-solutions/aplix-hygiene-fastening-solution/">Hygiene</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-640"><a href="https://www.skycorp.in/hook-and-loop-all-products/aplix-fastening-solutions/aplix-packaging-fastening-solution/">Packaging</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1347"><a href="#">Other Products<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-1282"><a href="https://www.skycorp.in/product-industry/webbings-tapes/">Webbings<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
		<ul class="sub-menu">
			<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1275"><a href="https://www.skycorp.in/hook-and-loop-all-products/webbings-tapes/nylon-webbings/">Nylon Webbings</a></li>
			<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-1276"><a href="https://www.skycorp.in/hook-and-loop-all-products/webbings-tapes/polyster-webbings/">Polyester Webbings</a></li>
		</ul>
</li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-621"><a href="https://www.skycorp.in/hook-and-loop-all-products/sky-other-products/sky-velvet-tapes-ribbons/">Velvet Tapes</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-620"><a href="https://www.skycorp.in/hook-and-loop-all-products/sky-other-products/sky-fiber-glass-insect-screen/">Insect Screen</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-622"><a href="https://www.skycorp.in/hook-and-loop-all-products/sky-other-products/elastics/">Elastics</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-633"><a href="https://www.skycorp.in/hook-and-loop-all-products/sky-other-products/neoprene-fabric/">Neoprene Fabrics</a></li>
	</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-398"><a href="#">Industry/Applications<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-694"><a href="https://www.skycorp.in/product-industry/footwear/">Footwear</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-parent current-product-parent menu-item-1045"><a href="https://www.skycorp.in/product-industry/orthopedic/">Orthopedic</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1046"><a href="https://www.skycorp.in/product-industry/defense-and-military-wear/">Defense</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1348"><a href="https://www.skycorp.in/product-industry/medical-equipments/">Medical Equipments</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-696"><a href="https://www.skycorp.in/product-industry/automotive/">Automotive</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-parent current-product-parent menu-item-1047"><a href="https://www.skycorp.in/product-industry/hook-and-loop-fasteners-for-sports-wear/">Sports</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1048"><a href="https://www.skycorp.in/product-industry/ppe/">PPE</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-695"><a href="https://www.skycorp.in/product-industry/packaging/">Packaging</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-parent current-product-parent menu-item-698"><a href="https://www.skycorp.in/product-industry/apparels/">Apparels</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-parent current-product-parent menu-item-699"><a href="https://www.skycorp.in/product-industry/infant-wear/">Infant Wear</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-700"><a href="https://www.skycorp.in/product-industry/cable-networking/">Cable Networking</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-701"><a href="https://www.skycorp.in/product-industry/fibc/">FIBC</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-702"><a href="https://www.skycorp.in/product-industry/aviation/">Aviation</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-704"><a href="https://www.skycorp.in/product-industry/hygiene-and-baby-care/">Hygiene</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-705"><a href="https://www.skycorp.in/product-industry/home-furnishing/">Home Furnishing</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-706"><a href="https://www.skycorp.in/product-industry/hook-and-loop-strap-for-luggage/">Luggage</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-707"><a href="https://www.skycorp.in/product-industry/abrasives/">Abrasives</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1050"><a href="https://www.skycorp.in/product-industry/stationery/">Stationery</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-708"><a href="https://www.skycorp.in/product-industry/media/">Media</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1049"><a href="https://www.skycorp.in/product-industry/warehousing/">Warehousing</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-397"><a href="https://www.skycorp.in/investor-relation/">Investor Relation<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1051"><a href="https://www.skycorp.in/board-of-directors/">Board of Directors</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1053"><a href="https://www.skycorp.in/appointment-of-independent-directors/">Terms Of Appointment Of Independent Directors</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1056"><a href="#">Financial Reports<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1052"><a href="https://www.skycorp.in/annual-reports/">Annual Reports</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1062"><a href="https://www.skycorp.in/annual-reports-2/">Quarterly Reports</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1064"><a href="https://www.skycorp.in/mgt-9-details-for-investors/">MGT-9</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1055"><a href="#">Policies and Code of Conduct<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1101"><a href="https://www.skycorp.in/sky-policies-adopted-2/">Sky Policies Adopted</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1057"><a href="#">Shareholder Information<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1067"><a href="https://www.skycorp.in/notice-avt/">Notice &#038; Avt</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1082"><a href="https://www.skycorp.in/results/">Results</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1071"><a href="https://www.skycorp.in/investor-contacts/">Investor Contacts</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1073"><a href="https://www.skycorp.in/iepf/">IEPF</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1076"><a href="https://www.skycorp.in/notice/">Notice</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1079"><a href="https://www.skycorp.in/corporate-annoucements/">Corporate Annoucements</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-715"><a href="#">News<span class="child-indicator"><svg width="8" height="8" viewBox="0 0 15 15"><path d="M2.1,3.2l5.4,5.4l5.4-5.4L15,4.3l-7.5,7.5L0,4.3L2.1,3.2z"/></svg></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-716"><a href="https://www.skycorp.in/hook-and-loop-blogs/">Blog</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-717"><a href="https://www.skycorp.in/hook-and-loop-uses-videos/">Videos</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1201"><a href="https://www.skycorp.in/hook-and-loop-online/">Shop</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-420"><a href="https://www.skycorp.in/hook-and-loop-tape-manufacturer-contact-us/">Contact Us</a></li>
</ul>
</nav>

</div></div></div></div></header>]]></script><script id="ct-header-template-mobile" type="text-template/mobile"><![CDATA[<header id="header" data-header="static" data-device="mobile" ><div data-row="middle" data-columns="3" ><div class="ct-container" ><div data-column="start" data-placements="2" ><div data-items="primary" >
<a
	href="#offcanvas"
	class="ct-header-trigger"
	data-design="simple"
	data-id="trigger" >

	<span class="ct-trigger" data-type="type-1">
		<span></span>
	</span>

	<span class="ct-label" hidden>
		Menu	</span>
</a>

</div><div data-items="secondary" >
<div
	class="ct-header-cta "
	data-id="button" >

	<a
		href="/request-a-quote/"
		class="ct-button"
		data-size="medium"
		target="_blank">
		Request A Quote	</a>
</div>
</div></div><div data-column="middle" ><div data-items="" >
<div
	class="site-branding"
	data-id="logo" 	>

	<h1 class="site-title" >
					<a href="https://www.skycorp.in/" class="custom-logo-link" rel="home"><img width="2598" height="1181" src="https://www.skycorp.in/wp-content/uploads/2020/05/Sky-logo-2.png" class="custom-logo" alt="Skycorp" /></a>			</h1>

	</div>

</div></div><div data-column="end" data-placements="1" ><div data-items="primary" >
<div
	class="ct-header-cart "
			data-id="cart" >

	<a class="ct-cart-item"
		href="https://www.skycorp.in/cart/"
				data-count="0">

		<span class="ct-cart-label">Cart</span>
		
		<i>
			<svg viewBox="0 0 10 10"><path d="M10,8.9L9.6,1c0-0.6-0.4-1-1.1-1H1.4c-0.6,0-1,0.4-1,1L0,8.9l0,0C0,9.6,0.4,10,1,10h7.9C9.6,10,10,9.6,10,8.9L10,8.9zM8.9,9.1H1C0.9,9.1,0.9,9.1,0.9,9L1.2,1l0,0c0-0.1,0-0.1,0.1-0.1h7.2c0.1,0,0.1,0.1,0.1,0.1l0,0l0.4,7.9C9.1,9.1,9.1,9.1,8.9,9.1zM6.5,1.8C6.2,1.8,6.1,2,6.1,2.2v1.3c0,0.6-0.4,1.1-1.1,1.1c-0.6,0-1-0.5-1-1.1V2.2c0-0.2-0.2-0.5-0.5-0.5S3,1.9,3,2.2v1.3c0,1.1,0.9,1.9,1.9,1.9s1.9-0.8,1.9-1.9V2.2C6.9,2,6.8,1.8,6.5,1.8z"/></svg>		</i>
	</a>
</div>
</div></div></div></div></header>]]></script><script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--woo yoast-schema-graph--footer'>{"@context":"https://schema.org","@graph":[{"@type":"Product","@id":"https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-micro-hook-moulded-injection-hook/#product","name":"Moulded/Injection Hook","url":"https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-micro-hook-moulded-injection-hook/","description":"Sky Micro Hook- Mouled/Injection Hook for Infant Wear\r\nSky Micro Hook is one of the premium hook products. It is extremely soft in texture that prevents any injury or rashes to baby skin. It is a skin-friendly product barring any allergic reactions to baby skin. It is utilized for closure in many Infant Wear Products, and Accessories like in Diapers, Nappies, and Bibs. It possesses material that ensures proper closure and safety to the baby.\r\n\r\nIt is highly recommended for the infant wear, it is also used in apparel and some specific automotive and ancillary applications.\r\n\r\nIt is mainly utilized in high strength applications.\r\n\r\n&nbsp;","image":{"@id":"http://35.203.161.219/product/sky-micro-hook-moulded-injection-hook/#primaryimage"},"offers":[{"@type":"Offer","price":"1.00","priceSpecification":{"price":"1.00","priceCurrency":"INR"},"priceCurrency":"INR","availability":"http://schema.org/InStock","url":"https://www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-micro-hook-moulded-injection-hook/","seller":{"@id":"https://www.skycorp.in/#organization"},"@id":"https://www.skycorp.in/#/schema/offer/582-0"}],"mainEntityOfPage":{"@id":"http://35.203.161.219/product/sky-micro-hook-moulded-injection-hook/#webpage"}}]}</script>

<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" aria-label="Close (Esc)"></button>
				<button class="pswp__button pswp__button--share" aria-label="Share"></button>
				<button class="pswp__button pswp__button--fs" aria-label="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" aria-label="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div>
			</div>
			<button class="pswp__button pswp__button--arrow--left" aria-label="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" aria-label="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
	<script type="text/javascript">var c=document.body.className;c=c.replace(/woocommerce-no-js/,'woocommerce-js');document.body.className=c;</script>
	<script type='text/javascript'>//<![CDATA[
var wc_single_product_params={"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"","zoom_options":[],"photoswipe_enabled":"1","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":""};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/frontend/single-product.min.js?ver=4.1.0'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript'>//<![CDATA[
var _wpUtilSettings={"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-includes/js/wp-util.min.js?ver=5.4.2'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript'>//<![CDATA[
var wc_add_to_cart_variation_params={"wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"Sorry, no products matched your selection. Please choose a different combination.","i18n_make_a_selection_text":"Please select some product options before adding this product to your cart.","i18n_unavailable_text":"Sorry, this product is unavailable. Please choose a different combination."};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=4.1.0'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/flexslider/jquery.flexslider.min.js?ver=2.7.2'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/themes/blocksy/static/bundle/events.js?ver=1.7.10'></script>
<script async type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/blocksy-companion/framework/extensions/woocommerce-extra/static/bundle/main.js?ver=1.7.5'></script>
<script type='text/javascript'>//<![CDATA[
var ct_localizations={"ajax_url":"https:\/\/www.skycorp.in\/wp-admin\/admin-ajax.php","nonce":"63450784f5","public_url":"https:\/\/www.skycorp.in\/wp-content\/themes\/blocksy\/static\/bundle\/","rest_url":"https:\/\/www.skycorp.in\/wp-json\/","search_url":"https:\/\/www.skycorp.in\/search\/QUERY_STRING\/","show_more_text":"Show more","more_text":"More","internet_explorer_template":"<div class=\"ct-unsuported-browser\">\n\t<svg width=\"120px\" height=\"120px\" viewBox=\"0 0 100 100\">\n\t\t<path d=\"M14.8,54.7c-1.3,0-2.3-1-2.3-2.3v-4.7c0-1.3,1-2.3,2.3-2.3s2.3,1,2.3,2.3v4.7C17.2,53.7,16.1,54.7,14.8,54.7zM35.6,79.6l-4.1-2.3c-1.1-0.6-2.6-0.2-3.2,0.9s-0.2,2.6,0.9,3.2l4.1,2.3c0.4,0.2,0.8,0.3,1.2,0.3c0.8,0,1.6-0.4,2-1.2C37.1,81.7,36.7,80.2,35.6,79.6z M68.4,77.3l-4.1,2.3c-1.1,0.6-1.5,2.1-0.9,3.2c0.5,0.8,1.2,1.2,2,1.2c0.4,0,0.8-0.1,1.2-0.3l4.1-2.3c1.1-0.6,1.5-2.1,0.9-3.2S69.5,76.6,68.4,77.3zM82.8,47.7v4.7c0,1.3,1,2.3,2.3,2.3s2.3-1,2.3-2.3v-4.7c0-1.3-1-2.3-2.3-2.3S82.8,46.3,82.8,47.7zM70.8,18.7l-4.1-2.3c-1.1-0.6-2.6-0.2-3.2,0.9s-0.2,2.6,0.9,3.2l4.1,2.3c0.4,0.2,0.8,0.3,1.2,0.3c0.8,0,1.6-0.4,2-1.2C72.3,20.8,71.9,19.3,70.8,18.7zM33.3,16.3l-4.1,2.3c-1.1,0.6-1.5,2.1-0.9,3.2c0.5,0.8,1.2,1.2,2,1.2c0.4,0,0.8-0.1,1.2-0.3l4.1-2.3c1.1-0.6,1.5-2.1,0.9-3.2S34.4,15.7,33.3,16.3z M60.2,10.2c0,5.6-4.5,10.2-10.2,10.2s-10.2-4.5-10.2-10.2S44.4,0,50,0S60.2,4.5,60.2,10.2zM55.5,10.2c0-3-2.4-5.5-5.5-5.5s-5.5,2.4-5.5,5.5s2.4,5.5,5.5,5.5S55.5,13.2,55.5,10.2z M60.2,89.8c0,5.6-4.5,10.2-10.2,10.2s-10.2-4.5-10.2-10.2S44.4,79.7,50,79.7S60.2,84.2,60.2,89.8zM55.5,89.8c0-3-2.4-5.5-5.5-5.5s-5.5,2.4-5.5,5.5s2.4,5.5,5.5,5.5S55.5,92.9,55.5,89.8zM95.3,70.3c0,5.6-4.5,10.2-10.2,10.2S75,75.9,75,70.3s4.5-10.2,10.2-10.2S95.3,64.7,95.3,70.3zM90.6,70.3c0-3-2.4-5.5-5.5-5.5s-5.5,2.4-5.5,5.5s2.4,5.5,5.5,5.5S90.6,73.4,90.6,70.3zM75,29.7c0-5.6,4.5-10.2,10.2-10.2s10.2,4.5,10.2,10.2s-4.5,10.2-10.2,10.2S75,35.3,75,29.7zM79.7,29.7c0,3,2.4,5.5,5.5,5.5s5.5-2.4,5.5-5.5s-2.4-5.5-5.5-5.5S79.7,26.6,79.7,29.7z M25,29.7c0,5.6-4.5,10.2-10.2,10.2S4.7,35.3,4.7,29.7s4.5-10.2,10.2-10.2S25,24.1,25,29.7zM20.3,29.7c0-3-2.4-5.5-5.5-5.5s-5.5,2.4-5.5,5.5s2.4,5.5,5.5,5.5S20.3,32.7,20.3,29.7zM25,70.3c0,5.6-4.5,10.2-10.2,10.2S4.7,75.9,4.7,70.3s4.5-10.2,10.2-10.2S25,64.7,25,70.3z M20.3,70.3c0-3-2.4-5.5-5.5-5.5s-5.5,2.4-5.5,5.5s2.4,5.5,5.5,5.5S20.3,73.4,20.3,70.3z\"\/>\n\t\t<path d=\"M64.6,52.7c0,1-1.9,3.9-4.8,5.3c-1.7,0.9-7-3.4-5.9-4.8s3.4,0.5,4.7-0.5C63.2,49.5,64.6,51.6,64.6,52.7zM71.9,50c0,12-9.8,21.9-21.9,21.9S28.1,62,28.1,50S38,28.1,50,28.1S71.9,38,71.9,50z M67.2,50c0-1.6-0.2-3.1-0.8-4.5l-1.2-0.3h-1.8c-1.1,0-2.1-0.5-2.7-1.3C59.8,42.7,59,41,58.4,41c-0.8,0-2.1,5.5-4.8,5.5c-2.1,0-3.9-4.6-3.3-6.2c0.9-2.3,2.8-1.8,4.8-2.7c0.9-0.4,1.5-1.3,1.5-2.3v-1.2c-2-0.9-4.2-1.3-6.6-1.3c-8.6,0-15.7,6.3-17.1,14.6l1.2-0.1c0.7,0.1,1.3,0.4,1.4,0.9c0.3,2-1.8,2.7-1.2,4.3c0.5,1.5,2.5,2,3.5,1c1-0.9,2.2-3,2.4-4.9c0.3-3.3,4.4-4.8,6.1-1.8c0.5,0.9,0.2,2.5-0.8,3.6s-2.7,2-2.6,3.4c0.2,1.2,4.8,1.4,3.4,6.7c-0.2,0.8-0.6,1.5-1,2.1c-0.3,0.5-0.7,0.9-1.2,1.3c-0.7,0.5-1.4,0.9-2,1.2H42c2.4,1.2,5.2,2,8,2C59.5,67.2,67.2,59.5,67.2,50z\"\/>\n\t<\/svg>\n\n\n\t<h2>\n\t\tYou are using an old or unsupported browser\t<\/h2>\n\n\t<p>\n\t\tFor <b><a href=\"https:\/\/mashable.com\/article\/internet-explorer-vulnerability-just-stop-using-it\/\" target=\"_blank\">security reasons<\/a><\/b> and to get the most out of your theme please upgrade to the latest version of <a href=\"https:\/\/www.microsoft.com\/en-us\/windows\/microsoft-edge\" target=\"_blank\">your browser<\/a> or use one of these browsers instead <a href=\"https:\/\/www.google.com\/chrome\/\" target=\"_blank\">Chrome<\/a>, <a href=\"https:\/\/support.apple.com\/downloads\/safari\" target=\"_blank\">Safari<\/a> or <a href=\"https:\/\/www.mozilla.org\/en-US\/firefox\/new\/\" target=\"_blank\">Firefox <\/a>.\t<\/p>\n<\/div>\n\n"};
//]]></script>
<script async type='text/javascript' src='https://www.skycorp.in/wp-content/themes/blocksy/static/bundle/main.js?ver=1.7.10'></script>
<script type='text/javascript'>//<![CDATA[
var blocksy_ext_instagram_localization={"ajax_url":"https:\/\/www.skycorp.in\/wp-admin\/admin-ajax.php","public_url":"https:\/\/www.skycorp.in\/wp-content\/plugins\/blocksy-companion\/framework\/extensions\/instagram\/static\/bundle\/"};
//]]></script>
<script async type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/blocksy-companion/framework/extensions/instagram/static/bundle/main.js?ver=1.7.5'></script>
<script async type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/blocksy-companion/framework/extensions/cookies-consent/static/bundle/main.js?ver=1.7.5'></script>
<script type='text/javascript'>//<![CDATA[
var wpcf7={"apiSettings":{"root":"https:\/\/www.skycorp.in\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.7'></script>
<script type='text/javascript'>//<![CDATA[
var wc_add_to_cart_params={"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/www.skycorp.in\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=4.1.0'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe.min.js?ver=4.1.1'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe-ui-default.min.js?ver=4.1.1'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript'>//<![CDATA[
var woocommerce_params={"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=4.1.0'></script>
<script type='text/javascript'>//<![CDATA[
var wc_cart_fragments_params={"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_ae5ae87cc1276019b04676350a16a780","fragment_name":"wc_fragments_ae5ae87cc1276019b04676350a16a780","request_timeout":"5000"};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=4.1.0'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-includes/js/comment-reply.min.js?ver=5.4.2'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-includes/js/wp-embed.min.js?ver=5.4.2'></script>
<script type="text/javascript">window.addEventListener("load",function(event){jQuery(".cfx_form_main,.wpcf7-form,.wpforms-form,.gform_wrapper form").each(function(){var form=jQuery(this);var screen_width="";var screen_height="";if(screen_width==""){if(screen){screen_width=screen.width;}else{screen_width=jQuery(window).width();}}if(screen_height==""){if(screen){screen_height=screen.height;}else{screen_height=jQuery(window).height();}}form.append('<input type="hidden" name="vx_width" value="'+screen_width+'">');form.append('<input type="hidden" name="vx_height" value="'+screen_height+'">');form.append('<input type="hidden" name="vx_url" value="'+window.location.href+'">');});});</script> 

  <div id="bitnami-banner" data-banner-id="c35a8">  <style>#bitnami-banner {z-index:100000;height:80px;padding:0px;width:120px;background:transparent;position:fixed;right:0px;bottom:0px;border:0px solid #EDEDED;} #bitnami-banner .bitnami-corner-image-div {position:fixed;right:0px;bottom:0px;border:0px;z-index:100001;height:110px;} #bitnami-banner .bitnami-corner-image-div .bitnami-corner-image {position:fixed;right:0px;bottom:0px;border:0px;z-index:100001;height:110px;} #bitnami-close-banner-button {height:12px;width:12px;z-index:10000000000;position:fixed;right:5px;bottom:65px;display:none;cursor:pointer}</style>  <img id="bitnami-close-banner-button" alt="Close Bitnami banner" src="https://www.skycorp.in/bitnami/images/close.png"/>  <div class="bitnami-corner-image-div">     <a href="../../../bitnami/index.html" target="_blank">       <img class="bitnami-corner-image" alt="Bitnami" src="https://www.skycorp.in/bitnami/images/corner-logo.png"/>     </a>  </div>  <script type="text/javascript" src="https://www.skycorp.in/bitnami/banner.js"></script> </div>   </body>

<!-- Mirrored from www.skycorp.in/hook-and-loop-all-products/speciality-hook-and-loop/sky-micro-hook-moulded-injection-hook/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 04 Jul 2020 05:14:46 GMT -->
</html>
