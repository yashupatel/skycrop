
<?php
include('include/header.php');
?>
	<main id="main" class="site-main">

		

	
	<div id="primary" class="content-area">

		<div class="ct-container">

			<section>
				
	<article id="post-14" class="post-14 page type-page status-publish hentry" data-structure="elementor:wide:normal">

		
		
		<div class="entry-content">
					<div data-elementor-type="wp-page" data-elementor-id="14" class="elementor elementor-14" data-elementor-settings="[]">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
							<section class="elementor-element elementor-element-24eaada ct-section-stretched elementor-section-full_width elementor-section-content-middle ct-columns-alignment-fix elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="24eaada" data-element_type="section">
						<div class="elementor-container elementor-column-gap-narrow">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-481ff46 elementor-column elementor-col-100 elementor-top-column" data-id="481ff46" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-170e92b elementor-arrows-position-inside elementor-pagination-position-outside elementor-widget elementor-widget-image-carousel" data-id="170e92b" data-element_type="widget" data-settings="{&quot;slides_to_show&quot;:&quot;1&quot;,&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;effect&quot;:&quot;slide&quot;,&quot;speed&quot;:500,&quot;direction&quot;:&quot;ltr&quot;}" data-widget_type="image-carousel.default">
				<div class="elementor-widget-container">
					<div class="elementor-image-carousel-wrapper swiper-container" dir="ltr">
			<div class="elementor-image-carousel swiper-wrapper">
				<div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="/valleyphoto.jpeg"
				height="100" width="1200" alt="Colored hook and loop tapes"/></figure></div><div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://www.skycorp.in/wp-content/uploads/elementor/thumbs/Colored-Hook-Loop-Tape-orvkfxsobpdimtmvlmiokleo6x0aa1oxd8aq7c33q8.jpg" alt="Colored-Hook-Loop-Tape"/></figure></div>			</div>
												<div class="swiper-pagination"></div>
													<div class="elementor-swiper-button elementor-swiper-button-prev">
						<i class="eicon-chevron-left" aria-hidden="true"></i>
						<span class="elementor-screen-only">Previous</span>
					</div>
					<div class="elementor-swiper-button elementor-swiper-button-next">
						<i class="eicon-chevron-right" aria-hidden="true"></i>
						<span class="elementor-screen-only">Next</span>
					</div>
									</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				
				<section class="elementor-element elementor-element-647aef10 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="647aef10" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-397291fa elementor-column elementor-col-50 elementor-top-column" data-id="397291fa" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-column-wrap  elementor-element-populated">
							<div class="elementor-background-overlay"></div>
					
			</div>
		</div>
				<div class="elementor-element elementor-element-1c3801db elementor-column elementor-col-50 elementor-top-column" data-id="1c3801db" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-66cfc8aa elementor-widget elementor-widget-heading" data-id="66cfc8aa" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">About us</h3>		</div>
				</div>
				<div class="elementor-element elementor-element-3308c95 elementor-widget elementor-widget-heading" data-id="3308c95" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h1 class="elementor-heading-title elementor-size-default">Hook and Loop Manufacturer and Supplier since 2014</h1>		</div>
				</div>
				<div class="elementor-element elementor-element-4f2f619d elementor-widget elementor-widget-text-editor" data-id="4f2f619d" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>We introduce ourselves as one of India’s leading player in narrow woven fabrics, (technical textiles). As one of the countries’s most trusted suppliers of narrow woven fabric, particularly Hook and loop tape fasteners, we have gained recognition as specialists in providing fastening solutions.</p></div>
					<br>
					<div class="elementor-text-editor elementor-clearfix"><p  >Valley textile has worked closely with some of the most reputed companies spread across various industry segments in footwear, Orthopaedic, defence, Textiles, automotive, etc., and now offers customers an unmatched array of fastening solutions and products – we cater to every need and deliver Fast, reliable and quality solutions. Our dedicated and experienced sales staff is primed to listen to our client’s every request and tailor the best possible Solution/product offering at the quickest turn around. Our goal for our clients is not just maximizing revenue but also expanding their customer base and Consequently their business, leading to long-term sustainable growth.
					We are confident that given your support and best wishes we will progress to the Zenith of Hook & loop tape industry also manufacturer of Inject Hook / Molded Hook</p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-e381cb2 elementor-mobile-align-center elementor-widget elementor-widget-button" data-id="e381cb2" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-3ab366c elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="3ab366c" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-1077214 elementor-column elementor-col-100 elementor-top-column" data-id="1077214" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-a38d89e elementor-widget elementor-widget-heading" data-id="a38d89e" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Industries We Serve</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-172304e elementor-widget elementor-widget-widgetkit-for-elementor-content-carousel" data-id="172304e" data-element_type="widget" data-widget_type="widgetkit-for-elementor-content-carousel.default">
				<div class="elementor-widget-container">
			
        <div class="content-carousel" wk-slider="center:false; sets:false; autoplay:false; autoplay-interval:5000;">
            <div class="wk-visible-toggle wk-light wk-position-relative " tabindex="-1">
                <ul class="wk-grid-medium wk-slider-items wk-child-width-1-5@m wk-child-width-1-2@s wk-child-width-1-5@l" wk-grid>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Footwear</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                     <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Abrasives</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                     <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Orthopedic</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                     <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Defense</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Automotive</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Sports</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">PPE</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Packaging</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Apparels</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Aviation</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Packaging</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Hygiene</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Home Furnishing</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Luggage</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Luggage</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                    <li class="wk-flex wk-flex-center wk-grid-match">
                        <div class="wk-card wk-card-default wk-margin-small-bottom">
                                <div class="wk-card-media-top wk-overflow-hidden">
                                    <a class="wk-display-block" href="product-industry/automotive/index.html" rel="nofollow">
                                        <img src="https://www.skycorp.in/wp-content/uploads/2020/07/Automotive3.jpg" alt="Automotive">  
                                    </a> 
                                </div>
                            <div class="wk-card-body">
                        	  	<h3 class="wk-card-title wk-margin-remove">
                                	<a href="product-industry/automotive/index.html" rel="nofollow">Medical Equipments</a>
                                </h3>  
                            </div>
                        </div>
                    </li>
                </ul>
            </div>










                                    <ul class="wk-slider-nav wk-dotnav wk-flex-center wk-margin-medium-top"></ul>
                
        </div>

        <script>jQuery(function($){if(!$('body').hasClass('wk-content-carousel')){$('body').addClass('wk-content-carousel');}});</script>

		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				
				
				<section class="elementor-element elementor-element-ac39d1d elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="ac39d1d" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-5596e41 elementor-column elementor-col-100 elementor-top-column" data-id="5596e41" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-5c3db7a elementor-widget elementor-widget-heading" data-id="5c3db7a" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Trusted By Brands Worldwide</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-029997f elementor-arrows-position-outside elementor-pagination-position-outside elementor-widget elementor-widget-image-carousel" data-id="029997f" data-element_type="widget" data-settings="{&quot;slides_to_show&quot;:&quot;5&quot;,&quot;image_spacing_custom&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:55,&quot;sizes&quot;:[]},&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;speed&quot;:500,&quot;direction&quot;:&quot;ltr&quot;}" data-widget_type="image-carousel.default">
				<div class="elementor-widget-container">
					<div class="elementor-image-carousel-wrapper swiper-container" dir="ltr">
			<div class="elementor-image-carousel swiper-wrapper">
				<div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://www.skycorp.in/wp-content/uploads/2020/05/Ossur-Logo.png" alt="Ossur-Logo"/></figure></div><div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://www.skycorp.in/wp-content/uploads/2020/05/Toyota-Logo.png" alt="Toyota-Logo"/></figure></div><div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://www.skycorp.in/wp-content/uploads/2020/05/Boeing-Logo-1.png" alt="Boeing-Logo (1)"/></figure></div><div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://www.skycorp.in/wp-content/uploads/2020/05/Wilson-Logo.png" alt="Wilson-Logo"/></figure></div><div class="swiper-slide"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://www.skycorp.in/wp-content/uploads/2020/05/stryker-Logo.png" alt="stryker-Logo"/></figure></div>			</div>
												<div class="swiper-pagination"></div>
													<div class="elementor-swiper-button elementor-swiper-button-prev">
						<i class="eicon-chevron-left" aria-hidden="true"></i>
						<span class="elementor-screen-only">Previous</span>
					</div>
					<div class="elementor-swiper-button elementor-swiper-button-next">
						<i class="eicon-chevron-right" aria-hidden="true"></i>
						<span class="elementor-screen-only">Next</span>
					</div>
									</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				
				<section class="elementor-element elementor-element-858de51 ct-columns-alignment-fix elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="858de51" data-element_type="section">
						<div class="elementor-container elementor-column-gap-wide">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-f590acf elementor-column elementor-col-33 elementor-top-column" data-id="f590acf" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-2b6862d elementor-widget elementor-widget-text-editor" data-id="2b6862d" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Sagittis nisl rhoncus mattis rhoncus urna neque viverra. In iaculis nunc sed augue lacus viverra vitae congue.</p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-eb95e2d elementor-position-left elementor-vertical-align-middle elementor-widget elementor-widget-image-box" data-id="eb95e2d" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img src="https://demo.creativethemes.com/blocksy/modern-shop/wp-content/uploads/2020/03/avatar-1.jpg" title="" alt=""/></figure><div class="elementor-image-box-content"><h6 class="elementor-image-box-title">Jason Wong</h6><p class="elementor-image-box-description">CEO of wongsite.com</p></div></div>		</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-335829c elementor-column elementor-col-33 elementor-top-column" data-id="335829c" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-6be0b32 elementor-widget elementor-widget-text-editor" data-id="6be0b32" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Sagittis nisl rhoncus mattis rhoncus urna neque viverra. In iaculis nunc sed augue lacus viverra vitae congue.</p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-191bf08 elementor-position-left elementor-vertical-align-middle elementor-widget elementor-widget-image-box" data-id="191bf08" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img src="https://demo.creativethemes.com/blocksy/modern-shop/wp-content/uploads/2020/03/avatar-2.jpg" title="" alt=""/></figure><div class="elementor-image-box-content"><h6 class="elementor-image-box-title">Jason Wong</h6><p class="elementor-image-box-description">CEO of wongsite.com</p></div></div>		</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-a8b8322 elementor-column elementor-col-33 elementor-top-column" data-id="a8b8322" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-177bf5e elementor-widget elementor-widget-text-editor" data-id="177bf5e" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Sagittis nisl rhoncus mattis rhoncus urna neque viverra. In iaculis nunc sed augue lacus viverra vitae congue.</p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-a16c72e elementor-position-left elementor-vertical-align-middle elementor-widget elementor-widget-image-box" data-id="a16c72e" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img src="https://demo.creativethemes.com/blocksy/modern-shop/wp-content/uploads/2020/03/avatar-3.jpg" title="" alt=""/></figure><div class="elementor-image-box-content"><h6 class="elementor-image-box-title">Jason Wong</h6><p class="elementor-image-box-description">CEO of wongsite.com</p></div></div>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
				</div>

							
		
		
	</article>

				</section>

			
		</div>

	</div>


				
	</main>

	<footer class="site-footer"><div data-row="top"><div class="ct-container-fluid" data-stack="tablet"><div data-column="socials">
<div class="ct-footer-socials ">

	
		<div class="ct-social-box" data-type="simple" data-size="custom" data-color="custom">
			
							
				<a href="https://www.facebook.com/hookandloopmanufacturer" target="_blank" data-network="facebook">
					
				<svg width="20px" height="20px" viewBox="0 0 20 20">
					<path d="M20,10.1c0-5.5-4.5-10-10-10S0,4.5,0,10.1c0,5,3.7,9.1,8.4,9.9v-7H5.9v-2.9h2.5V7.9C8.4,5.4,9.9,4,12.2,4c1.1,0,2.2,0.2,2.2,0.2v2.5h-1.3c-1.2,0-1.6,0.8-1.6,1.6v1.9h2.8L13.9,13h-2.3v7C16.3,19.2,20,15.1,20,10.1z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						Facebook					</span>
				</a>
							
				<a href="https://www.linkedin.com/company/sky-industries-ltd" target="_blank" data-network="linkedin">
					
				<svg width="20px" height="20px" viewBox="0 0 20 20">
					<path d="M18.6,0H1.4C0.6,0,0,0.6,0,1.4v17.1C0,19.4,0.6,20,1.4,20h17.1c0.8,0,1.4-0.6,1.4-1.4V1.4C20,0.6,19.4,0,18.6,0z M6,17.1h-3V7.6h3L6,17.1L6,17.1zM4.6,6.3c-1,0-1.7-0.8-1.7-1.7s0.8-1.7,1.7-1.7c0.9,0,1.7,0.8,1.7,1.7C6.3,5.5,5.5,6.3,4.6,6.3z M17.2,17.1h-3v-4.6c0-1.1,0-2.5-1.5-2.5c-1.5,0-1.8,1.2-1.8,2.5v4.7h-3V7.6h2.8v1.3h0c0.4-0.8,1.4-1.5,2.8-1.5c3,0,3.6,2,3.6,4.5V17.1z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						Instagram					</span>
				</a>
			
					</div>

	</div>

</div><div data-column="widget-area-1"><div class="widget_text ct-widget widget_custom_html"><div class="textwidget entry-content custom-html-widget"><!-- Begin Mailchimp Signup Form -->
<link href="http://cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
	/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="https://skycorp.us8.list-manage.com/subscribe/post?u=d7b761504b138559ff5198267&amp;id=9683e1b32c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Subscribe</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d7b761504b138559ff5198267_9683e1b32c" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup--></div></div></div></div></div><div data-row="middle"><div class="ct-container-fluid"><div data-column="menu">
<nav id="footer-menu" class="footer-menu">


</nav>
</div></div></div><div data-row="bottom"><div class="ct-container-fluid"><div data-column="copyright">
<div class="ct-footer-copyright">

	<p>Copyright © 2020 Skycorp - Developed By Amit</p></div>
</div></div></div></footer></div>


		<div id="search-modal" class="ct-panel" data-behaviour="modal">
			<div class="close-button">
				<span class="ct-trigger closed">
					<span></span>
				</span>
			</div>

			<div class="content-container" data-align="middle">
				

<form role="search" method="get" class="search-form" action="https://www.skycorp.in/" data-live-results>
	<div class="ct-search-input">
		<input type="search" class="modal-field" placeholder="Search" value="" name="s" autocomplete="off" title="Search Input"/>

		<button class="search-submit">
			<span hidden>Search</span>
			<i>
				<svg width="12px" height="12px" viewBox="0 0 24 24">

					<path d="M23.3,23.3c-0.9,0.9-2.3,0.9-3.2,0l-4-4c-1.6,1-3.6,1.7-5.7,1.7C4.7,21,0,16.3,0,10.5S4.7,0,10.5,0C16.3,0,21,4.7,21,10.5
					c0,2.1-0.6,4-1.7,5.7l4,4C24.2,21,24.2,22.5,23.3,23.3z M10.5,3C6.4,3,3,6.4,3,10.5S6.4,18,10.5,18c4.1,0,7.5-3.4,7.5-7.5
					C18,6.4,14.7,3,10.5,3z"/>
				</svg>
			</i>

			<span data-loader="circles"><span></span><span></span><span></span></span>
		</button>

		
		

	</div>
</form>


			</div>
		</div>

		
		<div id="account-modal" class="ct-panel" data-behaviour="modal">
			<div class="close-button">
				<span class="ct-trigger closed">
					<span></span>
				</span>
			</div>

			<div class="content-container" data-align="middle">
				
		<form name="loginform" id="loginform" action="https://www.skycorp.in/wp-login.php" method="post">
			
			<p class="login-username">
				<label for="user_login">Username or Email Address</label>
				<input type="text" name="log" id="user_login" class="input" value="" size="20"/>
			</p>
			<p class="login-password">
				<label for="user_pass">Password</label>
				<input type="password" name="pwd" id="user_pass" class="input" value="" size="20"/>
			</p>
			
			<p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever"/> Remember Me</label></p>
			<p class="login-submit">
				<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Log In"/>
				<input type="hidden" name="redirect_to" value="https://www.skycorp.in/"/>
			</p>
			
		</form>			</div>
		</div>

		
<a class="skip-link screen-reader-text" href="#primary">
Skip to content</a>

	

	<div class="cookie-notification ct-fade-in-start" data-period="forever" data-type="type-1">

		<div class="container">
							<div class="ct-cookies-content">We use cookies to ensure that we give you the best experience on our website.</div>
			
			<button type="submit" class="ct-accept">Accept</button>

							<button class="ct-close">×</button>
			
		</div>
	</div>
	
<div id="offcanvas" class="ct-panel" data-behaviour="left-side" data-device="mobile"><div class="close-button">
				<span class="ct-trigger closed">
					<span></span>
				</span>
			</div>


	
		<div class="ct-social-box" data-type="simple" data-size="custom" data-color="custom">
			
							
				<a href="https://www.facebook.com/hookandloopmanufacturer" target="_blank" data-network="facebook">
					
				<svg width="20px" height="20px" viewBox="0 0 20 20">
					<path d="M20,10.1c0-5.5-4.5-10-10-10S0,4.5,0,10.1c0,5,3.7,9.1,8.4,9.9v-7H5.9v-2.9h2.5V7.9C8.4,5.4,9.9,4,12.2,4c1.1,0,2.2,0.2,2.2,0.2v2.5h-1.3c-1.2,0-1.6,0.8-1.6,1.6v1.9h2.8L13.9,13h-2.3v7C16.3,19.2,20,15.1,20,10.1z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						Facebook					</span>
				</a>
							
				<a href="https://www.linkedin.com/company/sky-industries-ltd" target="_blank" data-network="linkedin">
					
				<svg width="20px" height="20px" viewBox="0 0 20 20">
					<path d="M18.6,0H1.4C0.6,0,0,0.6,0,1.4v17.1C0,19.4,0.6,20,1.4,20h17.1c0.8,0,1.4-0.6,1.4-1.4V1.4C20,0.6,19.4,0,18.6,0z M6,17.1h-3V7.6h3L6,17.1L6,17.1zM4.6,6.3c-1,0-1.7-0.8-1.7-1.7s0.8-1.7,1.7-1.7c0.9,0,1.7,0.8,1.7,1.7C6.3,5.5,5.5,6.3,4.6,6.3z M17.2,17.1h-3v-4.6c0-1.1,0-2.5-1.5-2.5c-1.5,0-1.8,1.2-1.8,2.5v4.7h-3V7.6h2.8v1.3h0c0.4-0.8,1.4-1.5,2.8-1.5c3,0,3.6,2,3.6,4.5V17.1z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						LinkedIn					</span>
				</a>
							
				<a href="https://www.youtube.com/channel/UCkzb_pQ2qqOpZjZMjUyrjag" target="_blank" data-network="youtube">
					
				<svg width="20" height="20" viewbox="0 0 20 20">
					<path d="M15,0H5C2.2,0,0,2.2,0,5v10c0,2.8,2.2,5,5,5h10c2.8,0,5-2.2,5-5V5C20,2.2,17.8,0,15,0z M14.5,10.9l-6.8,3.8c-0.1,0.1-0.3,0.1-0.5,0.1c-0.5,0-1-0.4-1-1l0,0V6.2c0-0.5,0.4-1,1-1c0.2,0,0.3,0,0.5,0.1l6.8,3.8c0.5,0.3,0.7,0.8,0.4,1.3C14.8,10.6,14.6,10.8,14.5,10.9z"/>
				</svg>
			
					<span class="ct-label" hidden="">
						YouTube					</span>
				</a>
			
					</div>

	
</div>
</section></div></div>

<script>jQuery('li#tab-title-description a').text('Product Details');jQuery(window).load(function(){jQuery('.ct-cart-actions').before('<div class="ct-header-cta" id="quotation_button" data-id="button"><a href="/request-a-quote/" class="ct-button" data-size="medium" target="_blank">Request A Quote</a></div>');jQuery('#quotation_button').hide();});jQuery('select#type').on('change',function(){var selectVal=jQuery("#type option:selected").val();if(selectVal=="Custom Dimensions"){jQuery('.ct-cart-actions').hide();jQuery('#quotation_button').show();}else{jQuery('#quotation_button').hide();jQuery('.ct-cart-actions').show();}});if(window.location.pathname=="/product/aplix-packaging-fastening-solution/"){jQuery('form.variations_form.cart').show();}</script>
<script id="ct-header-template-desktop" type="text-template/desktop"></script>	<script type="text/javascript">var c=document.body.className;c=c.replace(/woocommerce-no-js/,'woocommerce-js');document.body.className=c;</script>
	<link rel='stylesheet' id='widgetkit_bs-css' href='https://www.skycorp.in/wp-content/plugins/widgetkit-for-elementor/dist/css/bootstrap.css?ver=2.3.2' type='text/css' media='all'/>
<link rel='stylesheet' id='widgetkit_main-css' href='https://www.skycorp.in/wp-content/plugins/widgetkit-for-elementor/dist/css/widgetkit.css?ver=2.3.2' type='text/css' media='all'/>
<link rel='stylesheet' id='uikit-css' href='https://www.skycorp.in/wp-content/plugins/widgetkit-for-elementor/dist/css/uikit.custom.min.css?ver=2.3.2' type='text/css' media='all'/>
<script type='text/javascript'>//<![CDATA[
var wc_single_product_params={"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"","zoom_options":[],"photoswipe_enabled":"1","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":""};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/frontend/single-product.min.js?ver=4.1.0'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript'>//<![CDATA[
var _wpUtilSettings={"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-includes/js/wp-util.min.js?ver=5.4.2'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript'>//<![CDATA[
var wc_add_to_cart_variation_params={"wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"Sorry, no products matched your selection. Please choose a different combination.","i18n_make_a_selection_text":"Please select some product options before adding this product to your cart.","i18n_unavailable_text":"Sorry, this product is unavailable. Please choose a different combination."};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=4.1.0'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/flexslider/jquery.flexslider.min.js?ver=2.7.2'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/themes/blocksy/static/bundle/events.js?ver=1.7.10'></script>
<script async type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/blocksy-companion/framework/extensions/woocommerce-extra/static/bundle/main.js?ver=1.7.5'></script>
<script type='text/javascript'>//<![CDATA[
var ct_localizations={"ajax_url":"https:\/\/www.skycorp.in\/wp-admin\/admin-ajax.php","nonce":"63450784f5","public_url":"https:\/\/www.skycorp.in\/wp-content\/themes\/blocksy\/static\/bundle\/","rest_url":"https:\/\/www.skycorp.in\/wp-json\/","search_url":"https:\/\/www.skycorp.in\/search\/QUERY_STRING\/","show_more_text":"Show more","more_text":"More","internet_explorer_template":"<div class=\"ct-unsuported-browser\">\n\t<svg width=\"120px\" height=\"120px\" viewBox=\"0 0 100 100\">\n\t\t<path d=\"M14.8,54.7c-1.3,0-2.3-1-2.3-2.3v-4.7c0-1.3,1-2.3,2.3-2.3s2.3,1,2.3,2.3v4.7C17.2,53.7,16.1,54.7,14.8,54.7zM35.6,79.6l-4.1-2.3c-1.1-0.6-2.6-0.2-3.2,0.9s-0.2,2.6,0.9,3.2l4.1,2.3c0.4,0.2,0.8,0.3,1.2,0.3c0.8,0,1.6-0.4,2-1.2C37.1,81.7,36.7,80.2,35.6,79.6z M68.4,77.3l-4.1,2.3c-1.1,0.6-1.5,2.1-0.9,3.2c0.5,0.8,1.2,1.2,2,1.2c0.4,0,0.8-0.1,1.2-0.3l4.1-2.3c1.1-0.6,1.5-2.1,0.9-3.2S69.5,76.6,68.4,77.3zM82.8,47.7v4.7c0,1.3,1,2.3,2.3,2.3s2.3-1,2.3-2.3v-4.7c0-1.3-1-2.3-2.3-2.3S82.8,46.3,82.8,47.7zM70.8,18.7l-4.1-2.3c-1.1-0.6-2.6-0.2-3.2,0.9s-0.2,2.6,0.9,3.2l4.1,2.3c0.4,0.2,0.8,0.3,1.2,0.3c0.8,0,1.6-0.4,2-1.2C72.3,20.8,71.9,19.3,70.8,18.7zM33.3,16.3l-4.1,2.3c-1.1,0.6-1.5,2.1-0.9,3.2c0.5,0.8,1.2,1.2,2,1.2c0.4,0,0.8-0.1,1.2-0.3l4.1-2.3c1.1-0.6,1.5-2.1,0.9-3.2S34.4,15.7,33.3,16.3z M60.2,10.2c0,5.6-4.5,10.2-10.2,10.2s-10.2-4.5-10.2-10.2S44.4,0,50,0S60.2,4.5,60.2,10.2zM55.5,10.2c0-3-2.4-5.5-5.5-5.5s-5.5,2.4-5.5,5.5s2.4,5.5,5.5,5.5S55.5,13.2,55.5,10.2z M60.2,89.8c0,5.6-4.5,10.2-10.2,10.2s-10.2-4.5-10.2-10.2S44.4,79.7,50,79.7S60.2,84.2,60.2,89.8zM55.5,89.8c0-3-2.4-5.5-5.5-5.5s-5.5,2.4-5.5,5.5s2.4,5.5,5.5,5.5S55.5,92.9,55.5,89.8zM95.3,70.3c0,5.6-4.5,10.2-10.2,10.2S75,75.9,75,70.3s4.5-10.2,10.2-10.2S95.3,64.7,95.3,70.3zM90.6,70.3c0-3-2.4-5.5-5.5-5.5s-5.5,2.4-5.5,5.5s2.4,5.5,5.5,5.5S90.6,73.4,90.6,70.3zM75,29.7c0-5.6,4.5-10.2,10.2-10.2s10.2,4.5,10.2,10.2s-4.5,10.2-10.2,10.2S75,35.3,75,29.7zM79.7,29.7c0,3,2.4,5.5,5.5,5.5s5.5-2.4,5.5-5.5s-2.4-5.5-5.5-5.5S79.7,26.6,79.7,29.7z M25,29.7c0,5.6-4.5,10.2-10.2,10.2S4.7,35.3,4.7,29.7s4.5-10.2,10.2-10.2S25,24.1,25,29.7zM20.3,29.7c0-3-2.4-5.5-5.5-5.5s-5.5,2.4-5.5,5.5s2.4,5.5,5.5,5.5S20.3,32.7,20.3,29.7zM25,70.3c0,5.6-4.5,10.2-10.2,10.2S4.7,75.9,4.7,70.3s4.5-10.2,10.2-10.2S25,64.7,25,70.3z M20.3,70.3c0-3-2.4-5.5-5.5-5.5s-5.5,2.4-5.5,5.5s2.4,5.5,5.5,5.5S20.3,73.4,20.3,70.3z\"\/>\n\t\t<path d=\"M64.6,52.7c0,1-1.9,3.9-4.8,5.3c-1.7,0.9-7-3.4-5.9-4.8s3.4,0.5,4.7-0.5C63.2,49.5,64.6,51.6,64.6,52.7zM71.9,50c0,12-9.8,21.9-21.9,21.9S28.1,62,28.1,50S38,28.1,50,28.1S71.9,38,71.9,50z M67.2,50c0-1.6-0.2-3.1-0.8-4.5l-1.2-0.3h-1.8c-1.1,0-2.1-0.5-2.7-1.3C59.8,42.7,59,41,58.4,41c-0.8,0-2.1,5.5-4.8,5.5c-2.1,0-3.9-4.6-3.3-6.2c0.9-2.3,2.8-1.8,4.8-2.7c0.9-0.4,1.5-1.3,1.5-2.3v-1.2c-2-0.9-4.2-1.3-6.6-1.3c-8.6,0-15.7,6.3-17.1,14.6l1.2-0.1c0.7,0.1,1.3,0.4,1.4,0.9c0.3,2-1.8,2.7-1.2,4.3c0.5,1.5,2.5,2,3.5,1c1-0.9,2.2-3,2.4-4.9c0.3-3.3,4.4-4.8,6.1-1.8c0.5,0.9,0.2,2.5-0.8,3.6s-2.7,2-2.6,3.4c0.2,1.2,4.8,1.4,3.4,6.7c-0.2,0.8-0.6,1.5-1,2.1c-0.3,0.5-0.7,0.9-1.2,1.3c-0.7,0.5-1.4,0.9-2,1.2H42c2.4,1.2,5.2,2,8,2C59.5,67.2,67.2,59.5,67.2,50z\"\/>\n\t<\/svg>\n\n\n\t<h2>\n\t\tYou are using an old or unsupported browser\t<\/h2>\n\n\t<p>\n\t\tFor <b><a href=\"https:\/\/mashable.com\/article\/internet-explorer-vulnerability-just-stop-using-it\/\" target=\"_blank\">security reasons<\/a><\/b> and to get the most out of your theme please upgrade to the latest version of <a href=\"https:\/\/www.microsoft.com\/en-us\/windows\/microsoft-edge\" target=\"_blank\">your browser<\/a> or use one of these browsers instead <a href=\"https:\/\/www.google.com\/chrome\/\" target=\"_blank\">Chrome<\/a>, <a href=\"https:\/\/support.apple.com\/downloads\/safari\" target=\"_blank\">Safari<\/a> or <a href=\"https:\/\/www.mozilla.org\/en-US\/firefox\/new\/\" target=\"_blank\">Firefox <\/a>.\t<\/p>\n<\/div>\n\n"};
//]]></script>
<script async type='text/javascript' src='https://www.skycorp.in/wp-content/themes/blocksy/static/bundle/main.js?ver=1.7.10'></script>
<script type='text/javascript'>//<![CDATA[
var blocksy_ext_instagram_localization={"ajax_url":"https:\/\/www.skycorp.in\/wp-admin\/admin-ajax.php","public_url":"https:\/\/www.skycorp.in\/wp-content\/plugins\/blocksy-companion\/framework\/extensions\/instagram\/static\/bundle\/"};
//]]></script>
<script async type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/blocksy-companion/framework/extensions/instagram/static/bundle/main.js?ver=1.7.5'></script>
<script async type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/blocksy-companion/framework/extensions/cookies-consent/static/bundle/main.js?ver=1.7.5'></script>
<script type='text/javascript'>//<![CDATA[
var wpcf7={"apiSettings":{"root":"https:\/\/www.skycorp.in\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.7'></script>
<script type='text/javascript'>//<![CDATA[
var wc_add_to_cart_params={"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/www.skycorp.in\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=4.1.0'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript'>//<![CDATA[
var woocommerce_params={"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=4.1.0'></script>
<script type='text/javascript'>//<![CDATA[
var wc_cart_fragments_params={"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_ae5ae87cc1276019b04676350a16a780","fragment_name":"wc_fragments_ae5ae87cc1276019b04676350a16a780","request_timeout":"5000"};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=4.1.0'></script>
<script type='text/javascript'>//<![CDATA[
var localize={"ajaxurl":"https:\/\/www.skycorp.in\/wp-admin\/admin-ajax.php","nonce":"0d2da7e1b5"};
//]]></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/uploads/essential-addons-elementor/fa14246f25c1f49521caa3b833063c08.min.js?ver=1593839398'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-includes/js/wp-embed.min.js?ver=5.4.2'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/widgetkit-for-elementor/dist/js/widgetkit.js?ver=2.3.2'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/widgetkit-for-elementor/dist/js/uikit.min.js?ver=2.3.2'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/widgetkit-for-elementor/dist/js/uikit-icons.min.js?ver=2.3.2'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=2.9.8'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.7.6'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6'></script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=2.9.8'></script>
<script type='text/javascript'>var elementorFrontendConfig={"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","downloadImage":"Download image"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":690,"lg":1000,"xl":1440,"xxl":1600},"version":"2.9.8","urls":{"assets":"https:\/\/www.skycorp.in\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"general":{"elementor_global_image_lightbox":"yes","elementor_lightbox_enable_counter":"yes","elementor_lightbox_enable_fullscreen":"yes","elementor_lightbox_enable_zoom":"yes","elementor_lightbox_enable_share":"yes","elementor_lightbox_title_src":"title","elementor_lightbox_description_src":"description"},"editorPreferences":[]},"post":{"id":14,"title":"Hook%20and%20Loop%20Manufacturer%20%7C%20Hook%20and%20Loop%20Tapes%20%7C%20Sky%20Industries%20Ltd","excerpt":"","featuredImage":false}};</script>
<script type='text/javascript' src='https://www.skycorp.in/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=2.9.8'></script>
<script type="text/javascript">window.addEventListener("load",function(event){jQuery(".cfx_form_main,.wpcf7-form,.wpforms-form,.gform_wrapper form").each(function(){var form=jQuery(this);var screen_width="";var screen_height="";if(screen_width==""){if(screen){screen_width=screen.width;}else{screen_width=jQuery(window).width();}}if(screen_height==""){if(screen){screen_height=screen.height;}else{screen_height=jQuery(window).height();}}form.append('<input type="hidden" name="vx_width" value="'+screen_width+'">');form.append('<input type="hidden" name="vx_height" value="'+screen_height+'">');form.append('<input type="hidden" name="vx_url" value="'+window.location.href+'">');});});</script> 

  <div id="bitnami-banner" data-banner-id="c35a8">  <style>#bitnami-banner {z-index:100000;height:80px;padding:0px;width:120px;background:transparent;position:fixed;right:0px;bottom:0px;border:0px solid #EDEDED;} #bitnami-banner .bitnami-corner-image-div {position:fixed;right:0px;bottom:0px;border:0px;z-index:100001;height:110px;} #bitnami-banner .bitnami-corner-image-div .bitnami-corner-image {position:fixed;right:0px;bottom:0px;border:0px;z-index:100001;height:110px;} #bitnami-close-banner-button {height:12px;width:12px;z-index:10000000000;position:fixed;right:5px;bottom:65px;display:none;cursor:pointer}</style>  <img id="bitnami-close-banner-button" alt="Close Bitnami banner" src="https://www.skycorp.in/bitnami/images/close.png"/>  <div class="bitnami-corner-image-div">     <a href="bitnami/index.html" target="_blank">       <img class="bitnami-corner-image" alt="Bitnami" src="https://www.skycorp.in/bitnami/images/corner-logo.png"/>     </a>  </div>  <script type="text/javascript" src="https://www.skycorp.in/bitnami/banner.js"></script> </div>   </body>

<!-- Mirrored from www.skycorp.in/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 04 Jul 2020 05:10:14 GMT -->
</html>
